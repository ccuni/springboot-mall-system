package com.star.ms.common.vo;

import lombok.Data;

import java.util.List;

// 查询不同商品种类的评论数
@Data
public class ProductWithCategoryVo {
    private List<String> name;
    private List<Integer> total;
}
