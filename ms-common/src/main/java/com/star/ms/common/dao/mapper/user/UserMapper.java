package com.star.ms.common.dao.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.star.ms.common.entity.product.Product;
import com.star.ms.common.entity.user.User;
import com.star.ms.common.vo.UserProvinceCountVo;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_user】的数据库操作Mapper
* @createDate 2022-05-22 13:29:39
* @Entity com.star.ms.pojo.User
*/
@Repository
public interface UserMapper extends BaseMapper<User> {

    User selectByCodeAndPassword(String code, String password);

    int updateImgByCode(String code, String imgUrl);

    List<User> selectByMapWithRole(Map<String, Object> condition);

    IPage<Product> selectListWithRoleBySome(Page<Object> page, Map<String, Object> condition);

    List<User> selectListWithRoleByIds(List<Long> ids);

    int updateImgById(Long userId, String imgUrl);

    @Select("select max(user_id) from ms_user")
    int selectMaxUserId();
    @Select("select trim(replace(replace(replace(substring_index(user_address,'-',1),' ',''),'省',''),'市','')) name, count(*) value from ms_user GROUP BY substring_index(user_address,'-',1)")
    List<UserProvinceCountVo> selectListWithProvinceCount();

    User selectUserWithRole(String userCode);
}




