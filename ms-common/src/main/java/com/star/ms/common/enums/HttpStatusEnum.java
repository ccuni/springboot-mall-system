package com.star.ms.common.enums;

public enum HttpStatusEnum {
    SUCCESS(200, "请求成功!"),
    FAIL(500, "服务器内部出错."),
    NOT_FOUND(404, "找不到所指定的资源."),
    NOT_AUTH(401, "权限不足.");
    private final int code;
    private final String msg;
    private HttpStatusEnum(int code, String msg){
        this.code = code;
        this.msg = msg;
    }
    public int code(){ return code; }
    public String msg() { return msg; }
}
