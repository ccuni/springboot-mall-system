package com.star.ms.common.enums;


import lombok.Getter;

@Getter
public enum EmailEnum {
    YCC("uni222@yeah.net", "ycc的邮箱地址"),
    SYJ("syj62901@163.com", "SYJ的邮箱地址");
    private final String name;
    private final String detail;
    private EmailEnum(String name, String detail){
        this.name = name;
        this.detail = detail;
    }
}
