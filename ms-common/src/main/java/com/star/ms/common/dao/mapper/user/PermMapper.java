package com.star.ms.common.dao.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.star.ms.common.entity.user.Perm;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_perm】的数据库操作Mapper
* @createDate 2022-05-22 13:28:53
* @Entity com.star.ms.pojo.Perm
*/
@Repository
public interface PermMapper extends BaseMapper<Perm> {

    List<Perm> selectByRoleId(Integer roleId);

    IPage<Perm> selectPageByMap(Page<Perm> page, @Param("map") Map<String, Object> condition);
}




