package com.star.ms.common.entity.provider;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 *
 * @TableName ms_provider
 */
@TableName(value ="ms_provider")
@Data
public class Provider implements Serializable {
    /**
     *
     */
    @TableId(value = "provider_id", type = IdType.AUTO)
    private Long id;

    /**
     * 供应商名称
     */
    @TableField(value = "provider_name")
    private String name;

    /**
     * 供应商详细描述
     */
    @TableField(value = "provider_detail")
    private String detail;

    /**
     * 供应商联系人
     */
    @TableField(value = "provider_contact")
    private String contact;

    /**
     * 联系电话
     */
    @TableField(value = "provider_tel")
    private String tel;

    /**
     * 地址
     */
    @TableField(value = "provider_address")
    private String address;

    /**
     *
     */
    @TableField(value = "provider_img")
    private String img;

    /**
     * 供应商创建
     */
    @TableField(value = "provider_status")
    private String status;

    /**
     * 供应商创建时间
     */
    @TableField(value = "provider_create_time")
    private Date createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}