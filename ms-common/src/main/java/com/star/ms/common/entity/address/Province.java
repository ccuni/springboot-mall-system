package com.star.ms.common.entity.address;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName ms_province
 */
@TableName(value ="ms_province")
@Data
public class Province implements Serializable {
    /**
     * 
     */
    @TableId(value = "province_id", type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    @TableField(value = "province_code")
    private String code;

    /**
     * 
     */
    @TableField(value = "province_name")
    private String name;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}