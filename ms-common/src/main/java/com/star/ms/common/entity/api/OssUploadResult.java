package com.star.ms.common.entity.api;

import lombok.Data;

// OSS 前后端交互的返回值
@Data
public class OssUploadResult {
    // 文件唯一标识
    private String uid;
    // 文件地址
    private String imgUrl;
    // 状态有：uploading done error removed
    private String status;
    // 服务端响应内容，如：'{"status": "success"}'
    private String response;
}
