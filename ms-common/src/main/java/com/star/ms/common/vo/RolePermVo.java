package com.star.ms.common.vo;

import com.star.ms.common.entity.user.Perm;
import com.star.ms.common.entity.user.Role;
import lombok.Data;

import java.io.Serializable;

/**
 * 一个角色对应一个权限
 */
@Data
public class RolePermVo implements Serializable {
    private Role role;
    private Perm perm;
}
