package com.star.ms.common.entity.api;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * 验证码实体类
 */
@Data
@AllArgsConstructor
public class Captcha {
    private String info;
    private Date createTime;
    private long lastTime;
    public Captcha(String info, long createTime){
        this.info = info;
        this.createTime = new Date(createTime);
    }
}
