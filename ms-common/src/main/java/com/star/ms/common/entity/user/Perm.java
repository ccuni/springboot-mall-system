package com.star.ms.common.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName ms_perm
 */
@TableName(value ="ms_perm")
@Data
public class Perm implements Serializable {
    /**
     * 权限ID
     */
    @TableId(value = "perm_id", type = IdType.AUTO)
    private Integer id;

    /**
     * 权限名称
     */
    @TableField(value = "perm_name")
    private String name;

    /**
     * 权限允许访问的URL
     */
    @TableField(value = "perm_url")
    private String url;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}