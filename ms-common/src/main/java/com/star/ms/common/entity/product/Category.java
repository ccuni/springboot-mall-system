package com.star.ms.common.entity.product;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName ms_category
 */
@TableName(value ="ms_category")
@Data
public class Category implements Serializable {
    /**
     * 商品类型ID
     */
    @TableId(value = "category_id")
    private Long id;

    /**
     * 商品名称
     */
    @TableField(value = "category_name")
    private String name;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private Long sum;
}