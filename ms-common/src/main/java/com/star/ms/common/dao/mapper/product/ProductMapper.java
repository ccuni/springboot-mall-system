package com.star.ms.common.dao.mapper.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.star.ms.common.entity.product.Category;
import com.star.ms.common.entity.product.Product;
import com.star.ms.common.vo.CategoryCountVo;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_product】的数据库操作Mapper
* @createDate 2022-05-22 13:29:00
* @Entity com.star.ms.pojo.Product
*/
@Repository
public interface ProductMapper extends BaseMapper<Product> {

    List<Product> selectProductByMap(Page<Object> page, @Param("map") Map<String, Object> condition);
    IPage<Product> selectProductPageByMap(Page<Object> page, @Param("map") Map<String, Object> condition);

    Product selectWithCategoryAndProviderById(Long id);
    @Select("   select category_id, category_name, count(*) as total " +
            "        from ms_product left join ms_category " +
            "        on product_category_id = category_id group by category_id")
    List<CategoryCountVo> countByCategory();

    @Select("select category_id as id, category_name as name from ms_product, ms_category " +
            "where ms_product.product_category_id = ms_category.category_id" +
            " and ms_product.product_id = #{productId}")
    Category selectCategoryByProductId(Long productId);

    boolean updateImgById(Long productId, String imgUrl);
}




