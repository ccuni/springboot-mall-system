package com.star.ms.common.utils;

import com.star.ms.common.entity.api.Captcha;

import java.util.Random;

/**
 * 验证码工具类
 */
public class CaptchaUtil {
    private static final String KEYS = "0123456789";
    /**
     * 生成随机的n位验证码
     * @param n 验证码的位数
     * @return 返回 n位的随机验证码
     */
    public static Captcha getRandomCaptcha(int n){
        // 每次都根据当前的系统时间设置 随机种子
        Random random = new Random(System.currentTimeMillis());
        //验证码包含的字段，可自己设置
        char[] numbers = new char[n];
        // 生成n位的随机验证码
        for (int i = 0; i < n; i++) {
            numbers[i] = KEYS.charAt(random.nextInt(KEYS.length()));
        }
        return new Captcha(new String(numbers), System.currentTimeMillis());
    }
}
