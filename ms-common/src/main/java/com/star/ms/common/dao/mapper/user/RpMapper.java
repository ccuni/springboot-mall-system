package com.star.ms.common.dao.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.ms.common.entity.user.Rp;
import org.springframework.stereotype.Repository;

/**
* @author uni10
* @description 针对表【ms_rp】的数据库操作Mapper
* @createDate 2022-05-22 13:29:27
* @Entity com.star.ms.pojo.Rp
*/
@Repository
public interface RpMapper extends BaseMapper<Rp> {

}




