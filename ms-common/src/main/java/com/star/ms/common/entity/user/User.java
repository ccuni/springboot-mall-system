package com.star.ms.common.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @TableName ms_user
 */
@TableName(value ="ms_user")
@Data
public class User implements Serializable {
    /**
     * 
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户邮箱
     */
    @TableField(value = "user_email")
    private String email;

    /**
     * 用户编码
     */
    @TableField(value = "user_usercode")
    private String usercode;

    /**
     * 用户名称
     */
    @TableField(value = "user_username")
    private String username;

    /**
     * 用户密码（带密文）
     */
    @TableField(value = "user_password")
    private String password;

    /**
     * 性别（1: 女, 2:男）
     */
    @TableField(value = "user_gender")
    private Integer gender;

    /**
     * 生日
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @TableField(value = "user_birthday")
    private Date birthday;

    /**
     * 
     */
    @TableField(value = "user_tel")
    private String tel;

    /**
     * 地址
     */
    @TableField(value = "user_address")
    private String address;

    /**
     * 盐
     */
    @TableField(value = "user_salt")
    private String salt;

    /**
     * 用户头像
     */
    @TableField(value = "user_img")
    private String img;

    /**
     * 创建时间
     */
    @TableField(value = "user_create_time")
    private Date createTime;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private Role role;
}