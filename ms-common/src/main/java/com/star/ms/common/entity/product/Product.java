package com.star.ms.common.entity.product;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.star.ms.common.entity.provider.Provider;
import lombok.Data;

/**
 * 
 * @TableName ms_product
 */
@TableName(value ="ms_product")
@Data
public class Product implements Serializable {
    /**
     * 
     */
    @TableId(value = "product_id", type = IdType.AUTO)
    private Long id;

    /**
     * 商品类型的ID
     */
    @TableField(value = "product_category_id")
    private Long categoryId;

    /**
     * 供应商ID
     */
    @TableField(value = "product_provider_id")
    private Long providerId;

    /**
     * 货物名称
     */
    @TableField(value = "product_name")
    private String name;

    /**
     * 库存
     */
    @TableField(value = "product_stock")
    private Long stock;

    /**
     * 价格
     */
    @TableField(value = "product_price")
    private Double price;

    /**
     * 图片路径
     */
    @TableField(value = "product_img")
    private String img;

    /**
     * 描述
     */
    @TableField(value = "product_detail")
    private String detail;

    /**
     * 商品创建时间
     */
    @TableField(value = "product_create_time")
    private Date createTime;

    /**
     * 商品的状态
     */
    @TableField(value = "product_status")
    private Integer status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private Category category;

    @TableField(exist = false)
    private Provider provider;
}