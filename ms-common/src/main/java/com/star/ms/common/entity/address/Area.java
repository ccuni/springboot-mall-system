package com.star.ms.common.entity.address;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 
 * @TableName ms_area
 */
@TableName(value ="ms_area")
@Data
@AllArgsConstructor
public class Area implements Serializable {
    /**
     * 
     */
    @TableId(value = "area_id", type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    @TableField(value = "area_code")
    private String code;

    /**
     * 
     */
    @TableField(value = "area_name")
    private String name;

    /**
     * 
     */
    @TableField(value = "area_city_code")
    private String cityCode;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}