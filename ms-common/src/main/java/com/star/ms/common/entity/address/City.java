package com.star.ms.common.entity.address;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName ms_city
 */
@TableName(value ="ms_city")
@Data
public class City implements Serializable {
    /**
     * 
     */
    @TableId(value = "city_id", type = IdType.AUTO)
    private Integer id;

    /**
     * 
     */
    @TableField(value = "city_code")
    private String code;

    /**
     * 
     */
    @TableField(value = "city_name")
    private String name;

    /**
     * 
     */
    @TableField(value = "city_province_code")
    private String provinceCode;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}