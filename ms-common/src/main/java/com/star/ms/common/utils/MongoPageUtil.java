package com.star.ms.common.utils;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;
import java.util.regex.Pattern;

public class MongoPageUtil {
    private static int size = 5;
    private static int pages = 1;
    private static int currentPage = 1;

    public static void start(int page, int sz, Query q) {
        size = sz == 0 ? 6 : sz;
        currentPage = page;
        q.limit(size);
        q.skip((long) (currentPage - 1) * size);
    }
    public static <T>Page<T> getPage(List<T> list) {
        Page<T> page = new Page<>();
        page.setRecords(list);
        page.setCurrent(currentPage);
        page.setTotal(list.size() / size);
        page.setPages(pages);
        page.setSize(size);
        return page;
    }
    public static <T>Page<T> getPage(long total, List<T> list) {
        Page<T> page = new Page<>();
        page.setRecords(list);
        page.setCurrent(currentPage);
        page.setTotal(total);
        pages = (int)total / size;
        page.setPages(pages);
        page.setSize(size);
        return page;
    }

    /**
     * 用于模糊查询忽略大小写
     * @param string
     * @return
     */
    public Pattern getPattern(String string) {
        return Pattern.compile("^.*" + string + ".*$", Pattern.CASE_INSENSITIVE);
    }

}
