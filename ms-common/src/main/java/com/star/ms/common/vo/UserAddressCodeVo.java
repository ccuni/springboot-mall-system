package com.star.ms.common.vo;

import lombok.Data;

/**
 * 存储省市区的编号
 */
@Data
public class UserAddressCodeVo {
    private String provinceCode;
    private String cityCode;
    private String areaCode;
}
