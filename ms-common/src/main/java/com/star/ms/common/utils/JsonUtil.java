package com.star.ms.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import org.apache.commons.io.FileUtils;
import org.springframework.core.io.ClassPathResource;
import java.util.List;
import java.io.File;
import java.io.IOException;

public class JsonUtil {
    /**
     * 加载类资源文件下的数组形式的JSON数据
     * @param resourcesFileName JSON 文件名称
     * @return JSONArray ，即数组形式 JSON对象，底层是使用 List 存储
     */
    public static JSONArray load(String resourcesFileName) throws IOException {
        ClassPathResource resource = new ClassPathResource(resourcesFileName);
        File file = resource.getFile();
        String str = FileUtils.readFileToString(file);
        return  JSON.parseArray(str);
    }

    /**
     * 加载资源文件下的 json 数据文件，返回 List 结果
     * @param resourcesFileName  JSON 数据文件
     * @param clazz JSON文件中每一个项对应的实体类
     * @return  List
     * @param <T> JSON的实体类
     * @throws IOException 读取文件错误抛出的异常
     */
    public static <T>List<T> load(String resourcesFileName, Class<T> clazz) throws IOException {
        return load(resourcesFileName).toJavaList(clazz);
    }
}
