package com.star.ms.common.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName ms_ur
 */
@TableName(value ="ms_ur")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Ur implements Serializable {
    /**
     * 用户角色表ID
     */
    @TableId(value = "ur_id", type = IdType.AUTO)
    private Long id;

    /**
     * 用户ID
     */
    @TableField(value = "ur_user_id")
    private Long userId;

    /**
     * 该用户对应的角色ID
     */
    @TableField(value = "ur_role_id")
    private Integer roleId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;

    public Ur(Long userId, Integer roleId){
        this.userId = userId;
        this.roleId = roleId;
    }
}