package com.star.ms.common.entity.user;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.Data;

/**
 * 
 * @TableName ms_rp
 */
@TableName(value ="ms_rp")
@Data
public class Rp implements Serializable {
    /**
     * 角色权限表ID
     */
    @TableId(value = "rp_id", type = IdType.AUTO)
    private Integer id;

    /**
     * 角色ID
     */
    @TableField(value = "rp_role_id")
    private Integer roleId;

    /**
     * 该角色对应的权限ID
     */
    @TableField(value = "rp_perm_id")
    private Integer permId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}