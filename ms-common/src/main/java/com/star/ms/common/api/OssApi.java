package com.star.ms.common.api;

import com.aliyun.oss.*;
import com.aliyun.oss.model.ListObjectsRequest;
import com.aliyun.oss.model.OSSObjectSummary;
import com.aliyun.oss.model.ObjectListing;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.LinkedList;
import java.util.List;

@Configuration
public class OssApi {
    public static final String ENDPOINT = "https://oss-cn-hangzhou.aliyuncs.com";
    // 阿里云账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM用户进行API访问或日常运维，请登录RAM控制台创建RAM用户。
    public static final String ACCESS_KEY_ID = "LTAI5tNVd7a3Lx573sz7ZeJL";
    public static final String ACCESS_KEY_SECRET = "BQmOggb6QTX5ooD5Ar1NSCr2nJMDxA";
    public static final String BUCKET_NAME = "uni1024";
    public static final String URL_PREFIX = "https://" + BUCKET_NAME + ".oss-cn-hangzhou.aliyuncs.com/";

    @Bean
    public OSS OSSClient(){
        return new OSSClientBuilder().build(ENDPOINT, ACCESS_KEY_ID, ACCESS_KEY_SECRET);
    }
}
