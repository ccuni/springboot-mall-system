package com.star.ms.common.vo;

import lombok.Data;

import java.util.List;

/**
 * Dashboard 实体类
 */
@Data
public class CategoryCountVo {
    private String categoryName;
    private Integer total;
}
