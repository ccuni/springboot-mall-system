package com.star.ms.common.dao.mapper.user;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.star.ms.common.entity.user.Role;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_role】的数据库操作Mapper
* @createDate 2022-05-22 13:29:22
* @Entity com.star.ms.pojo.Role
*/
@Repository
public interface RoleMapper extends BaseMapper<Role> {

    Role selectByUserCode(String userCode);

    IPage<Role> selectWithPermsByCondition(Page<Object> page, @Param("map") Map<String, Object> condition);
}




