package com.star.ms.common.dao.mapper.product;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.ms.common.entity.product.Category;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
* @author uni10
* @description 针对表【ms_category】的数据库操作Mapper
* @createDate 2022-05-22 13:28:37
* @Entity com.star.ms.pojo.Category
*/

@Repository
public interface CategoryMapper extends BaseMapper<Category> {

    List<Category> selectAllWithCount();
}




