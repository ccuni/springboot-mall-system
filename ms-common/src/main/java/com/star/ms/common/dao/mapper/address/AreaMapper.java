package com.star.ms.common.dao.mapper.address;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.ms.common.entity.address.Area;
import org.springframework.stereotype.Repository;

/**
* @author uni10
* @description 针对表【ms_area】的数据库操作Mapper
* @createDate 2022-05-22 13:28:21
* @Entity com.star.ms.pojo.Area
*/
@Repository
public interface AreaMapper extends BaseMapper<Area> {

}




