package com.star.ms.common.api;

import com.star.ms.common.enums.EmailEnum;
import com.star.ms.common.entity.api.Captcha;
import com.star.ms.common.entity.user.User;
import com.star.ms.common.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

@Component
public class EmailApi {
    @Autowired
    private JavaMailSender javaMailSender;

    /**
     * 发送纯文本的邮件
     * @param from 邮件发送人
     * @param to   邮件接收人
     * @param subject   邮件主题
     * @param text  邮件内容
     */
    public boolean sendSimpleMail(String from, String to, String subject, String text) {
        SimpleMailMessage simpleMailMessage = new SimpleMailMessage();
        // 发件人
        simpleMailMessage.setFrom(from);
        // 收件人
        simpleMailMessage.setTo(to);
        // 邮件主题
        simpleMailMessage.setSubject(subject);
        // 邮件内容
        simpleMailMessage.setText(text);
        javaMailSender.send(simpleMailMessage);
        return true;
    }

    /**
     * 发送支持 html的邮件
     * @param from 邮件发送人
     * @param to   邮件接收人
     * @param subject   邮件主题
     * @param text  邮件内容
     */
    public boolean sendHtmlMail(String from, String to, String subject, String text) {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        try {
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
            // 1. 设置邮件发送人【固定为连接的邮箱账号】
            helper.setFrom(from);
            // 2. 设置邮件接受人【可以是任何的邮箱地址】
            helper.setTo(to);
            // 3. 设置邮箱主题
            helper.setSubject(subject);
            // 4. 设置邮件内容，第二个参数设置为 true， 表示让邮件支持 text/html 类型
            helper.setText(text, true);
            // 5. 发送邮件
            javaMailSender.send(mimeMessage);
            return true;
        } catch (MessagingException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 发送注册的邮件
     * @param user  注册用户信息
     * @param captcha  验证码
     * @return
     */
    public boolean sendRegisterMail(User user, Captcha captcha){
        // 获取用户的邮箱
        String to = user.getEmail();
        // 限制1. 不允许用户的邮箱为空
        if("".equals(to)) return false;
        String text = "    <div style=\"text-align:center; \n" +
                "    background-color: pink;\n" +
                "    padding: 50px;\n" +
                "    border-radius: 10px;\">\n" +
                "        <h1>========温馨提示======</h1>\n" +
                "        <h1>欢迎注册ms超市购物系统</h1>\n" +
                "        <h2>这是您的验证码: <span style=\"color:blue\">"+captcha.getInfo()+"</span></h2>\n" +
                "        <p>该验证码在<span style=\"color:red\"> 60秒 </span>内有效</p>\n" +
                "        <strong>"+ DateUtil.format(captcha.getCreateTime())+"</p>\n" +
                "    </div>";
        return sendHtmlMail(EmailEnum.YCC.getName(), to, "来自: ms购物系统的提示", text);
    }
    // 找回密码
    public boolean sendRetrieveMail(User user, Captcha captcha){
        // 获取用户的邮箱
        String to = user.getEmail();
        // 限制1. 不允许用户的邮箱为空
        if("".equals(to)) return false;
        String text = "    <div style=\"text-align:center; \n" +
                "    background-color: pink;\n" +
                "    padding: 50px;\n" +
                "    border-radius: 10px;\">\n" +
                "        <h1>========温馨提示======</h1>\n" +
                "        <h1>我们已收到您找回密码的请求</h1>\n" +
                "        <h2>这是您的验证码: <span style=\"color:blue\">"+captcha.getInfo()+"</span></h2>\n" +
                "        <p>该验证码在<span style=\"color:red\"> 60秒 </span>内有效</p>\n" +
                "        <strong>"+ DateUtil.format(captcha.getCreateTime())+"</p>\n" +
                "    </div>";
        return sendHtmlMail(EmailEnum.YCC.getName(), to, "来自: ms购物系统的提示", text);
    }
}
