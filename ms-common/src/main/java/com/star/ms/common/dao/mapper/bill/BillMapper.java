package com.star.ms.common.dao.mapper.bill;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.ms.common.entity.Bill;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_bill】的数据库操作Mapper
* @createDate 2022-06-07 07:33:29
* @Entity com.star.ms.common.pojo.Bill
*/
@Repository
public interface BillMapper extends BaseMapper<Bill> {

    IPage<Bill> selectWithBuyerAndProviderByPage(Page<Object> page, @Param("map") Map<String, Object> condition);

    Bill selectWithBuyerAndProviderAndProduct(Long billId);

    List<Bill> selectCurrentBills(Integer num);

    Double selectCountMoney();

    Float selectCountMoneyByCategoryId(Long categoryId);
}




