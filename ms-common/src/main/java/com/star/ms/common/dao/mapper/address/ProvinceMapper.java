package com.star.ms.common.dao.mapper.address;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.star.ms.common.entity.address.Province;
import org.springframework.stereotype.Repository;

/**
* @author uni10
* @description 针对表【ms_province】的数据库操作Mapper
* @createDate 2022-05-22 13:29:15
* @Entity com.star.ms.pojo.Province
*/
@Repository
public interface ProvinceMapper extends BaseMapper<Province> {

}




