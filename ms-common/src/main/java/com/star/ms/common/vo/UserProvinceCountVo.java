package com.star.ms.common.vo;

import lombok.Data;

@Data
public class UserProvinceCountVo {
    private String name;        // 省份名称
    private Integer value;      // 用户注册统计数
}
