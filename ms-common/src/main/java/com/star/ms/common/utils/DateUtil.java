package com.star.ms.common.utils;

import org.apache.ibatis.javassist.Loader;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 日期操作相关的工具类
 */
public class DateUtil {
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy年 MM月 dd 日 hh:mm:ss");
    public static String format(Date date){
        return sdf.format(date);
    }

    public static Date now(){ return new Date(System.currentTimeMillis());}

    public static Date parse(String pattern, String date){
        try {
            return new SimpleDateFormat(pattern).parse(date);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

}
