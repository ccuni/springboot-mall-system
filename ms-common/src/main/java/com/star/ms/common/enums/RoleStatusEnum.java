package com.star.ms.common.enums;

import lombok.Getter;

/**
 * 关于角色信息的枚举类（应该和数据库一致）
 */
@Getter
public enum RoleStatusEnum {

    VISITOR(1, "游客"),
    COMMON_USER(2, "普通用户"),
    PROVIDER(3, "供应商"),
    ADMIN(4, "管理员"),
    MANAGER(5, "经理");
    private final Integer id;
    private final String name;
    private RoleStatusEnum(Integer id, String name){
        this.id = id;
        this.name = name;
    }

}
