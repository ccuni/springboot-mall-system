package com.star.ms.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.star.ms.common.entity.product.Product;
import com.star.ms.common.entity.provider.Provider;
import com.star.ms.common.entity.user.User;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @TableName ms_bill
 */
@TableName(value ="ms_bill")
@Data
public class Bill implements Serializable {
    /**
     * 订单ID
     */
    @TableId(value = "bill_id", type = IdType.AUTO)
    private Long id;

    /**
     * 商品的ID
     */
    @TableField(value = "bill_product_id")
    private Long productId;

    /**
     * 供应商ID
     */
    @TableField(value = "bill_provider_id")
    private Long providerId;

    /**
     * 成交价格
     */
    @TableField(value = "bill_price")
    private Double price;

    /**
     * 交易数量
     */
    @TableField(value = "bill_number")
    private Integer number;

    /**
     * 交易时间
     */
    @TableField(value = "bill_create_time")
    private Date createTime;

    /**
     * 订单状态（0未支付，1已支付，2已完成）
     */
    @TableField(value = "bill_status")
    private Integer status;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
    @TableField(exist = false)
    private User buyer;
    @TableField(exist = false)
    private Product product;
    @TableField(exist = false)
    private Provider provider;
}