
var $Tz_wrap = $('#Tz_wrap');
var $upLoad = $('#upload');
var $input = $('#input');
var $rR = $('#rotateRightBtn');
var $rL = $('#rotateLeftBtn');
var cropper;
function useImgPlugin(width, height, cropWidth, cropHeight){
    init(width, height, cropWidth, cropHeight);
//拖拽上传
    drag();
//图片操作
    oper();
}
//初始化

//打开本地图片
function init(width, height, cropWidth, cropHeight) {
    //绑定
    var imgData = '';
    cropper = new ImageCropper(width, height, cropWidth, cropHeight);
    cropper.setCanvas("Tz_wrap");
    cropper.addPreview("plugin-imgUpload-final-img");
    //检测用户浏览器是否支持imagecropper插件
    if (!cropper.isAvaiable()) {
        alert("您的浏览器并不支持图像剪裁");
    };
    $('body').on({
        //进入
        dragenter: function (e) {
            e = e || window.event;
            e.preventDefault();
            e.stopPropagation();
        },
        //离开
        dragleave: function (e) {
            e = e || window.event;
            e.preventDefault();
            e.stopPropagation();
        },
        //在内部移动
        dragover: function (e) {
            e = e || window.event;
            e.preventDefault();
            e.stopPropagation();
        },
        drop: function (e) {
            e = e || window.event;
            e.preventDefault();
            e.stopPropagation();
        },
    })
}
function drag() {
    $Tz_wrap.get(0).ondrop = function (e) {
        e.preventDefault();
        //获取拖过来的文件
        var fs = e.dataTransfer.files;
        var _type = fs[0].type;
        if (_type.indexOf("image") != -1) {//判断他是不是图片文件
            var fd = new FileReader();
            cropper.loadImage(fs[0])
            $('.ptit').css({zIndex: -1})
            fd.readAsDataURL(fs[0]);
        }
    }
}
function oper() {
    /*上传图片点击事件 start*/
    $upLoad.click(function () {
        $input.click()
    })
    $input.click(function () {
        $(this).change(function () {
            // $('.ptit').css({zIndex: -1})
            selectImage(this.files)
        })
    })
    function selectImage(fileList) {
        cropper.loadImage(fileList[0])
    }
    /*上传图片点击事件 end*/
    /*旋转图片 start*/
    $rL.click(function () {
        cropper.rotate(-90)
    });
    $rR.click(function () {
        cropper.rotate(90)
    });
    /*旋转图片 end*/
}

// 保存自己的头像
function saveUserHeadBySelf(btn_id){
    ms_btn_disable(btn_id, '保存中')
    setTimeout(function (){
        $.ajax({
            url: '/user/img/self',
            type: 'PUT',
            data: {'img64Base': cropper.getCroppedImageData(150, 150)},
            dataType: 'json',
            error: (err)=>{
                ms_show_toast(false, '保存头像的AJAX请求错误' + err.responseText)
                console.error(err.responseText)
            },
            success: (result) => {
                if(result['code'] === CODE_SUCCESS){
                    ms_btn_enable(btn_id, result['msg'])
                    ms_show_toast_callback(true, result['msg'], function (){
                        location = '/user-center.html?type=head'
                    })
                } else {
                    ms_show_toast(false, ms_btn_enable)
                    ms_btn_enable(btn_id, '保存')
                }
            }
        })
    }, DELAY_TOAST_WHEN_BTN)
}

// 保存其他用户的头像
function saveUserHeadById(btn_id, user_id){
    ms_btn_disable(btn_id, '保存中')
    setTimeout(function (){
        $.ajax({
            url: '/user/img/' + user_id,
            type: 'PUT',
            data: {'img64Base': cropper.getCroppedImageData(150, 150)},
            dataType: 'json',
            error: (err)=>{
                ms_show_toast(false, '保存头像的AJAX请求错误' + err.responseText)
                console.error(err.responseText)
            },
            success: (result) => {
                if(result['code'] === CODE_SUCCESS){
                    ms_btn_enable(btn_id, result['msg'])
                    ms_show_toast_callback(true, result['msg'], function (){
                        location.reload()
                    })
                } else {
                    ms_show_toast(false, ms_btn_enable)
                    ms_btn_enable(btn_id, '保存')
                }
            }
        })
    }, DELAY_TOAST_WHEN_BTN)
}

// 保存商品图片
function saveProductImg(btn, productId, width, height){
    setTimeout(function (){
        $.ajax({
            url: '/product/img/save/' + productId,
            type: 'PUT',
            data: {'img64Base': cropper.getCroppedImageData(width, height)},
            dataType: 'json',
            error: (err)=>{
                ms_show_toast(false, '权限不足')
                console.error(err.responseText)
            },
            success: (result) => {
                if(result['code'] === CODE_SUCCESS){
                    ms_show_toast_callback(true, result['msg'], function (){
                        location.reload()
                    })
                } else {
                    ms_show_toast(false, ms_btn_enable)
                }
            }
        })
    }, DELAY_TOAST_WHEN_BTN)
}