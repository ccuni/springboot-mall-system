setTimeout(function (){
    $('.placeholder').removeClass('placeholder')
}, DELAY_LOAD_USER_HEAD)
setTimeout(function (){
    $('.placeholder-wave').removeClass('placeholder-wave')
}, DELAY_LOAD_USER_HEAD_WAVE)
/**
 * 向后台发送请求获取OSS的用户默认头像图片信息，
 * 返回值是一个 JSON格式的数组
 * [ {头像1名称, 头像1url}, {头像2名称, 头像2url}, ...]
 * @returns {*[]}
 */
function getOssImages(){
    let imgs = []
    $.ajax({
        url: '/user/oss',
        async: false,
        type: 'POST',
        dataType: 'json',
        error: function (err){
            ms_show_toast(false, '获取OSS图像的AJAX请求失败')
            console.error(err.responseText)
        },
        success: function (result){
            if(result['code'] === CODE_SUCCESS){
                imgs = result['data']
            } else
                ms_show_toast(false, '未成功获取OSS图像')
        }
    })
    return imgs
}

/**
 * 显示默认头像的弹窗
 */
function showDefaultHeadModal(){
    chose_img = ''
    let imgs = getOssImages()
    $ul = $('#ms-modal-user-default-img ul')
    // 动态渲染当前 OSS 存在的头像
    $ul.html(``)
    $.each(imgs, (index, img)=>{
        if(img.length === 2) {
            let name = img[0]
            let url = img[1]
            // 一行五个 ，<tr> 表示换行
            $ul.append(`<li class="w-auto mt-3 mb-3"><img src="${url}" class="ms-img-small"></li>`)
        }
    })
    $ul.append(`</tbody>`)
    // 设置监听
    $('.ms-img-small').click(function (){
        // 先移除所有的动态
        $('.ms-img-small').removeClass('ms-img-choose')
        // 再启动当前点击的动态
        $(this).addClass('ms-img-choose')
    })
}

/**
 * 保存用户头像
 */
function saveUserHead(btn_id){
    ms_btn_disable(btn_id, '保存中')
    setTimeout(function(){
        $.ajax({
            url: '/user/img/self',
            type: 'POST',
            data: {'imgUrl': $('.ms-img-choose').attr('src')},
            dataType: 'json',
            error: function (err) {
                ms_show_toast(false, '保存用户头像的AJAX请求失败')
                console.error(err.responseText)
            },
            success: function (result){
                if(result['code'] === CODE_SUCCESS){
                    ms_show_toast_callback(true, result['msg'], function (){
                        window.location.reload()
                    })
                    ms_btn_enable(btn_id, '保存')
                }
                else
                    ms_show_toast(false, result['msg'])
            }
        })
    }, DELAY_TOAST_WHEN_BTN)
}
