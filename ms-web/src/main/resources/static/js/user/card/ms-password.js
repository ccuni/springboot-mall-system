setTimeout(function (){
    $('.placeholder').removeClass('placeholder')
}, DELAY_LOAD_USER_PASSWORD)
let $form_password = $('#user-card-password-form')
let $password = $('#user-card-password-form-password')
let $password_second = $('#user-card-password-form-password-second')
let $captcha = $('#user-card-password-form-captcha')
let flag_password = false
let flag_password_second = false
let flag_captcha = false
$password.blur(function (){
    $errorText = $('#user-card-password-form-password-fail')
    let errorText = ''
    let value = $password.val()
    let reg = /^[0-9a-zA-Z]{6,15}$/
    flag_password = true
    // 1. 不能为空
    if(value === '') {
        flag_password = false
        errorText = '提示: 密码不能为空!'
    }
    // 2. 字符长不能超过10
    else if (value.length < 6 || value.length >= 15) {
        flag_password = false
        errorText = '提示: 密码长度需在6到15的范围内'
    }
    // 3. 只能是数字或字母，不能包含特殊符号
    else if(reg.test(value) === false) {
        flag_password = false
        errorText = '提示: 密码只能是数字或字母，不能包含特殊符号'
    }
    if(flag_password === false) {
        $password.removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
    else {
        $password.removeClass('is-invalid').addClass('is-valid')
    }
})
$password_second.blur(function (){
    $errorText = $('#user-card-password-form-password-second-fail')
    let errorText = ''
    let value = $password_second.val()
    flag_password_second = true
    if (value === '') {
        flag_password_second = false
        errorText = '提示: 确认密码不能为空'
    } else if ($password.val() !== value) {
        flag_password_second = false
        errorText = '提示: 两次输入的密码不一致'
    }
    if (flag_password_second === true) {
        $password_second.removeClass('is-invalid').addClass('is-valid')
    } else {
        $errorText.html(errorText)
        $password_second.removeClass('is-valid').addClass('is-invalid')
    }
})
$captcha.blur(function (){
    $errorText = $('#user-card-password-form-captcha-fail')
    let errorText = ''
    let value = $captcha.val()
    flag_captcha = true
    if(value === ''){
        flag_captcha = false
        errorText = '提示: 验证码不能为空'
    } else if(value.length > 6){
        flag_captcha = false
        errorText = '提示: 验证码不超过六位数'
    }
    if (flag_captcha === true) {
        $captcha.removeClass('is-invalid').addClass('is-valid')
    } else {
        $errorText.html(errorText)
        $captcha.removeClass('is-valid').addClass('is-invalid')
    }
})

function sendCaptcha(btn_id){
    $password.blur()
    $password_second.blur()
    if(flag_password === false || flag_password_second === false){
        ms_show_toast(false, '请输入格式正确的密码')
        return ;
    }
    ms_btn_disable(btn_id, '发送中')
    // 请求发送验证码
    setTimeout(function (){
        $.ajax({
            url: '/user/sendcode',
            type: 'POST',
            dataType: 'json',
            success: function (result){
                console.log(result)
                if(result['code'] === CODE_SUCCESS){
                    ms_show_toast(true, result['msg'])
                    // 加载倒计时按钮
                    ms_btn_code_wait(btn_id)
                } else
                    ms_show_toast(false, result['msg'])
            },
            error: function (error){
                console.log(error)
                ms_show_toast(false, "发送失败，请检查邮箱是否正确!")
                ms_btn_enable(btn_id)
            }
        })
    }, DELAY_REGISTER)
}
// 保存用户的密码
function saveUserPassword(btn_id){
    $password.blur()
    $password_second.blur()
    $captcha.blur()
    if(flag_password === false || flag_password_second === false || flag_captcha === false){
        ms_show_toast(false, '请填写正确的信息')
        return ;
    }
    ms_btn_disable(btn_id, '修改中')
    setTimeout(function (){
        $.ajax({
            url: '/user/updatePassword',               // 请求的controller
            type: 'POST',                       // 请求方式
            data: $form_password.serialize(),
            dataType: 'json',                   // 返回的数据类型
            success: function(result) {         // 返回成功后
                ms_btn_enable(btn_id, '修改')
                console.log(result)
                if(result['code'] === CODE_SUCCESS) {
                    // 修改密码成功 刷新页面
                    ms_show_toast_callback(true, result['msg'] + ", 请重新登录", function (){
                        exitLogin('/login.html')
                    })
                }
                else
                    ms_show_toast(false, result['msg'])
            },
            error: function(error){     // 若返回错误，则执行这个函数
                ms_show_toast(false, '修改失败, 服务端内部出错')
                // 控制台打印错误
                console.log(error)
            }
        })
    }, DELAY_REGISTER)
}