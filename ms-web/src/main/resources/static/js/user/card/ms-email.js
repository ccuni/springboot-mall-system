setTimeout(function (){
    $('.placeholder').removeClass('placeholder')
}, DELAY_LOAD_USER_EMAIL)
let $form_email_first = $('#user-card-email-form-first')
let $form_email_second = $('#user-card-email-form-second')
let $email_first = $('#user-card-email-form-first-email')
let $email_second = $('#user-card-email-form-second-email')
let flag_email = false
let flag_email_second = false
$email_first.blur(function (){
    $errorText = $('#user-card-email-form-first-email-fail')
    let value = $email_first.val()
    let errorText = ''
    let reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/g
    flag_email = true
    if(value.length === 0){
        flag_email = false
        errorText = '提示: 邮箱不能为空'
    }
    else if(value.length > 30){
        flag_email = false
        errorText = '提示: 邮箱的长度不能超过30'
    }
    else if(!reg.test(value)) {
        flag_email = false
        errorText = '提示: 输入的邮箱必须符合xxx@xxx.xxx的格式，且不超过30个字符'
    }
    if (flag_email === false){
        $errorText.html(errorText)
        $email_first.removeClass('is-valid').addClass('is-invalid')
    } else
        $email_first.removeClass('is-invalid').addClass('is-valid')
})
$email_second.blur(function (){
    $errorText = $('#user-card-email-form-second-email-fail')
    let value = $(this).val()
    let errorText = ''
    let reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/g
    flag_email_second = true
    if(value.length === 0){
        flag_email_second = false
        errorText = '提示: 邮箱不能为空'
    }
    else if(value.length > 30){
        flag_email_second = false
        errorText = '提示: 邮箱的长度不能超过30'
    }
    else if(!reg.test(value)) {
        flag_email_second = false
        errorText = '提示: 输入的邮箱必须符合xxx@xxx.xxx的格式，且不超过30个字符'
    }
    if (flag_email_second === false){
        $errorText.html(errorText)
        $(this).removeClass('is-valid').addClass('is-invalid')
    } else
        $(this).removeClass('is-invalid').addClass('is-valid')
})
/**
 * 第一次发送验证码验证旧的邮箱
 * @param btn_id
 */
function sendCaptchaFirst(btnId){
    $email_first.blur()
    if(flag_email === false){
        ms_show_toast(false, '请输入正确的邮箱!')
        return ;
    }
    ms_btn_disable(btnId, '发送中')
    setTimeout(function (){
        // 请求发送验证码
        $.ajax({
            url: '/user/sendCodeWithVerification',
            type: 'POST',
            data: $form_email_first.serialize(),
            dataType: 'json',
            success: function (result){
                if(result['code'] === CODE_SUCCESS){
                    ms_show_toast(true, result['msg'])
                    // 加载倒计时按钮
                    ms_btn_code_wait(btnId)
                } else {
                    ms_show_toast(false, result['msg'])
                    ms_btn_enable(btnId, '重新发送')
                }
            },
            error: function (error){
                console.error(error)
                ms_show_toast(false, "发送失败，请检查邮箱是否正确!")
                ms_btn_enable(btnId)
            }
        })
    }, DELAY_REGISTER)
}

/**
 * 第二次发送验证码给新的邮箱
 * @param btn_id
 */
function sendCaptchaSecond(btn_id){
    $email_second.blur()
    if(flag_email_second === false){
        ms_show_toast(false, '请输入正确的邮箱!')
        return ;
    }
    ms_btn_disable(btn_id, '发送中')
    setTimeout(function (){
        // 请求发送验证码
        $.ajax({
            url: '/user/sendCaptchaToUpdate',
            type: 'POST',
            data: $form_email_second.serialize(),
            dataType: 'json',
            success: function (result){
                ms_btn_enable(btn_id, '发送完成')
                if(result['code'] === CODE_SUCCESS){
                    ms_show_toast(true, result['msg'])
                    // 加载倒计时按钮
                    ms_btn_code_wait(btn_id)
                } else {
                    ms_show_toast(false, result['msg'])
                    ms_btn_enable(btn_id, '重新发送')
                }
            },
            error: function (error){
                console.error(error)
                ms_show_toast(false, "发送失败，请检查邮箱是否正确!")
                ms_btn_enable(btn_id)
            }
        })
    }, DELAY_REGISTER)
}
// 确认邮箱正确性
function saveUserEmailNext(btnId){
    ms_btn_disable(btnId, '验证中')
    setTimeout(function (){
        $.ajax({
            url: '/user/verifyEmailCode',
            type: 'POST',
            data: $form_email_first.serialize(),
            dataType: 'json',
            success: function(result) {
                ms_btn_enable(btnId, '下一步')
                if(result['code'] === CODE_SUCCESS) {
                    ms_show_toast_callback(true, result['msg'], function (){
                        // 关闭之前的验证码显示任务
                        ms_btn_code_no_wait('发送验证码')
                        // 开启设置新邮箱的模态框
                        $('#user-card-email-open-modal').click()
                    })
                }
                else {
                    ms_show_toast(false, result['msg'])
                    ms_btn_enable(btnId, '重新验证')
                }
            },
            error: function(error){
                ms_show_toast(false, '验证失败, 服务端内部出错')
                // 控制台打印错误
                console.log(error)
            }
        })
    }, DELAY_EMAIL_NEXT)
}
// 修改邮箱
function saveUserEmailOver(btn_id){
    ms_btn_disable(btn_id, '保存中')
    // 基于JQuery3.x版本的AJAX模板
    setTimeout(function (){
        $.ajax({
            url: '/user/updateEmail',
            type: 'POST',
            data: $form_email_second.serialize(),
            dataType: 'json',
            success: function(result) {
                ms_btn_enable(btn_id, '保存完成')
                console.log(result)
                if(result['code'] === CODE_SUCCESS) {
                    ms_show_toast_callback(true, result['msg'] + ', 请重新登录', function (){
                        exitLogin('/login.html')
                    })
                }
                else {
                    ms_show_toast(false, result['msg'])
                    ms_btn_enable(btn_id, '保存')
                }
            },
            error: function(error){
                ms_show_toast(false, '验证失败, 服务端内部出错')
                // 控制台打印错误
                console.log(error)
            }
        })
    }, DELAY_EMAIL_NEXT)
}