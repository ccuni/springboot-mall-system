/**
 * 设置预加载
 */
setTimeout(function () {
    // 关闭加载类
    $('.placeholder').removeClass('placeholder')
},DELAY_LOAD_LOGIN)

let $email = $('#form-email')
let $password = $('#form-password')
let $passwordSecond = $('#form-password-second')
let $emailCode = $('#form-email-code')

let flagEmail = false
let flagEmailCode = false
let flagPassword = false
let flagPasswordSecond = false

// 发送邮箱验证码
function sendEmailCode(btnId, form){
    let $form = $(form)
    if(!checkForm($form)) {
        ms_show_toast(false, '请填写规范的信息')
        return
    }
    ms_btn_disable(btnId, '发送中')

    setTimeout(function (){
        let result = ajaxCustom('POST', {
            'url': '/user/sendEmailCode',
            'data': $form.serialize()
        })
        if(result['code'] === CODE_SUCCESS){
            ms_show_toast(true, result['msg'])
            // 加载倒计时按钮
            ms_btn_code_wait(btnId)
        } else
            ms_show_toast(false, result['msg'])
    }, 400)
}
// 验证用户输入的验证码
function checkEmailCode(){
    let $errorText = $emailCode.next().next().next()
    let errorText = ''
    let value = $emailCode.val()
    flagEmailCode = true
    if(value === ''){
        flagEmailCode = false
        errorText = '验证码不能为空.'
    } else if (value !== captcha_trueth){
        flagEmailCode = false
        errorText = '验证码错误!'
    }
    if(flagEmailCode === false) {
        $emailCode.removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
    else {
        $emailCode.removeClass('is-invalid').addClass('is-valid')
    }
}
$email.blur(function (){
    let $errorText = $email.next().next().next()   // 错误提示
    let errorText = ''
    let value = $email.val()
    let reg = /^[0-9a-zA-Z]{5,11}$/
    flagEmail = true
    // 1. 不能为空
    if(value === '') {
        flagEmail = false
        errorText = '提示: 账号不能为空!'
    }
    // 2. 字符长不能超过10
    else if (value.length < 5 || value.length >= 10) {
        flagEmail = false
        errorText = '提示: 账号长度不能超过10且不能的低于5'
    }
    // 3. 只能是数字、字母、下划线，不能包含特殊符号
    else if(reg.test(value) === false) {
        flagEmail = false
        errorText = '提示: 账号只能是数字或字母，不能包含特殊符号'
    }
    if(flagEmail === false) {
        $(this).removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
    else {
        $(this).removeClass('is-invalid').addClass('is-valid')
    }
})
$password.blur(function (){
    let $errorText = $(this).next().next().next()
    let errorText = ''
    let value = $password.val()
    let reg = /^[\da-zA-Z]{6,15}$/
    flagPassword = true
    // 1. 不能为空
    if(value === '') {
        flagPassword = false
        errorText = '提示: 密码不能为空!'
    }
    // 2. 字符长不能超过10
    else if (value.length < 6 || value.length >= 15) {
        flagPassword = false
        errorText = '提示: 密码长度需在6到15的范围内'
    }
    // 3. 只能是数字或字母，不能包含特殊符号
    else if(reg.test(value) === false) {
        flagPassword = false
        errorText = '提示: 密码只能是数字或字母，不能包含特殊符号'
    }
    if(flagPassword === false) {
        $password.removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
    else {
        $password.removeClass('is-invalid').addClass('is-valid')
    }
})
$passwordSecond.blur(function (){
    let $errorText = $(this).next().next().next()
    let errorText = ''
    let value = $(this).val()
    let reg = /^[\da-zA-Z]{6,15}$/
    flagPasswordSecond = true
    // 1. 不能为空
    if(value === '') {
        flagPasswordSecond = false
        errorText = '提示: 密码不能为空!'
    }
    // 2. 必须和第一次的密码相等
    else if ($password.val() !== $passwordSecond.val()) {
        flagPasswordSecond = false
        errorText = '提示: 两次输入的密码不一致'
    }
    // 3. 只能是数字或字母，不能包含特殊符号
    else if(reg.test(value) === false) {
        flagPasswordSecond = false
        errorText = '提示: 密码只能是数字或字母，不能包含特殊符号'
    }
    if(flagPasswordSecond === false) {
        $(this).removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
    else {
        $(this).removeClass('is-invalid').addClass('is-valid')
    }
})
$email.blur(function (){

    let $errorText = $(this).next().next().next()
    let errorText = ''
    let reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/g
    let value = $(this).val()
    flagEmail = true
    if(value.length === 0){
        flagEmail = false
        errorText = '提示: 邮箱不能为空'
    }
    else if(value.length > 30){
        flagEmail = false
        errorText = '提示: 邮箱的长度不能超过30'
    }
    else if(!reg.test(value)) {
        flagEmail = false
        errorText = '提示: 输入的邮箱必须符合xxx@xxx.xxx的格式，且不超过30个字符'
    }
    if (flagEmail === false){
        $errorText.html(errorText)
        $email.removeClass('is-valid').addClass('is-invalid')
    } else
        $email.removeClass('is-invalid').addClass('is-valid')
})

function checkForm($form){
    $email.blur()
    $password.blur()
    $passwordSecond.blur()
    return flagEmail === true && flagPassword === true && flagPasswordSecond === true
}

// 找回账号
function retrieveUser(btnId){
    $passwordSecond.blur()
    if(!checkForm($('#' + btnId).data('bs-target')) || !flagPasswordSecond) {
        ms_show_toast(false, '请填写规范的信息')
        return;
    }
    ms_btn_disable(btnId, '重置中')
    setTimeout(function (){
        $.ajax({
            url: '/user/retrieve',       // 请求的controller
            type: 'POST',                // 请求方式
            data: $('#form-retrieve').serialize(),                // 传递表单的信息$ ('#form-msRegister').serialize()
            dataType: 'json',           // 返回的数据类型
            success: function(result) { // 返回成功后
                ms_btn_enable(btnId)
                console.log(result)
                if(result['code'] === CODE_SUCCESS) {
                    // 找回成功 -> 显示提示窗，显示后跳转到登录页面
                    ms_show_toast_callback(true, result['msg'], function (){
                        window.location = '/index.html'
                    })
                }
                else ms_show_toast(false, result['msg'])
                ms_btn_enable(btnId, '重置')
            },
            error: function(error){     // 若返回错误，则执行这个函数
                ms_show_toast(false, '登录出错，请刷新页面')
                // 控制台打印错误
                console.log(error)
            }
        })
    }, DELAY_LOGIN)
}

// 回车登录
$(document).keypress(function(event){
    if(event.keyCode == '13')
        $('.ms-keydown_enter').click()
})
