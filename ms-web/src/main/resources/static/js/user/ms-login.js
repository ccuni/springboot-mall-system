/**
 * 设置预加载
 */
setTimeout(function () {
    // 关闭加载类
    $('.placeholder').removeClass('placeholder')
},DELAY_LOAD_LOGIN)

let flag_code = false
let flag_password = false
let flag_login = false
let flag_captcha = false

$code = $('#ms-form-login-code')
$password = $('#ms-form-login-password')
$form_login = $('#ms-form-login')
$captcha = $('#ms-form-login-captcha')

// 验证用户输入的账号(是否存在)
function ms_login_check_code(){
    $errorText = $('#ms-form-login-check-code-fail')   // 错误提示
    let errorText = ''
    let value = $code.val()
    let reg = /^[0-9a-zA-Z]{5,11}$/
    flag_code = true
    // 1. 不能为空
    if(value === '') {
        flag_code = false
        errorText = '提示: 账号不能为空!'
    }
    // 2. 字符长不能超过10
    else if (value.length < 5 || value.length >= 10) {
        flag_code = false
        errorText = '提示: 账号长度不能超过10且不能的低于5'
    }
    // 3. 只能是数字、字母、下划线，不能包含特殊符号
    else if(reg.test(value) === false) {
        flag_code = false
        errorText = '提示: 账号只能是数字或字母，不能包含特殊符号'
    }

    if(flag_code === true) {
        $.ajax({
            url: '/user/verify',
            type: 'POST',
            data: $('#ms-form-login').serialize(),
            dataType: 'json',
            async: false,
            success: function(result) {
                if(result['code'] === CODE_SUCCESS){
                    // 账号存在 , 验证通过
                    $code.removeClass('is-invalid').addClass('is-valid')
                } else{
                    flag = false
                    errorText = '提示: 账号未注册'
                }
            },
            error: function (error){
                ms_show_toast(false, '查看用户是否存在请求失败')
                console.error(error)
            }
        })
    }
    if(flag_code === true){
        $code.removeClass('is-invalid').addClass('is-valid')
    } else{
        $code.removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
}
// 验证用户输入的密码
function ms_login_check_password(){
    $errorText = $('#ms-form-login-password-check')
    let errorText = ''
    let value = $password.val()
    let reg = /^[0-9a-zA-Z]{6,15}$/
    flag_password = true
    // 1. 不能为空
    if(value === '') {
        flag_password = false
        errorText = '提示: 密码不能为空!'
    }
    // 2. 字符长不能超过10
    else if (value.length < 6 || value.length >= 15) {
        flag_password = false
        errorText = '提示: 密码长度需在6到15的范围内'
    }
    // 3. 只能是数字或字母，不能包含特殊符号
    else if(reg.test(value) === false) {
        flag_password = false
        errorText = '提示: 密码只能是数字或字母，不能包含特殊符号'
    }
    if(flag_password === false) {
        $password.removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
    else {
        $password.removeClass('is-invalid').addClass('is-valid')
    }
}
// 验证用户输入的验证码
function ms_login_check_captcha(){
    $errorText = $('#ms-form-login-captcha-fail')
    let errorText = ''
    let value = $captcha.val()
    flag_captcha = true
    if(value === ''){
        flag_captcha = false
        errorText = '验证码不能为空.'
    } else if (value !== captcha_trueth){
        flag_captcha = false
        errorText = '验证码错误!'
    }
    if(flag_captcha === false) {
        $captcha.removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
    else {
        $captcha.removeClass('is-invalid').addClass('is-valid')
    }
}
$code.blur(ms_login_check_code)
$password.blur(ms_login_check_password)
$captcha.blur(ms_login_check_captcha)

function checkForm($form, ...expect){
    $code.blur()
    $password.blur()
    $captcha.blur()
    return  flag_code === true &&  flag_password === true && flag_captcha === true
}

function ms_login(btn_id){
    // 检查表单是否合理
    if(!checkForm($form_login)) return ;

    ms_btn_disable(btn_id, '登录中')
    setTimeout(function (){
        $.ajax({
            url: '/user/login',       // 请求的controller
            type: 'POST',                // 请求方式
            data: $('#ms-form-login').serialize(),                // 传递表单的信息
            dataType: 'json',           // 返回的数据类型
            success: function(result) { // 返回成功后
                ms_btn_enable(btn_id)
                console.log(result)
                if(result['code'] === CODE_SUCCESS) {
                    // 注册成功 -> 显示提示窗，显示后跳转到首页
                    ms_show_toast_callback(true, result['msg'], function (){
                        window.location = '/admin'
                    })
                }
                else {
                    ms_show_toast(false, result['msg'])
                }
                ms_btn_enable(btn_id, '登录')
            },
            error: function(error){     // 若返回错误，则执行这个函数
                ms_show_toast(false, '登录出错，请刷新页面')
                // 控制台打印错误
                console.log(error)
            }
        })
    }, DELAY_LOGIN)
}

// 回车登录
$(document).keypress(function(event){
    if(event.keyCode == '13')
        $('.ms-keydown_enter').click()
})
