$('.ms-list-group li a').click(function (){
    $(this).parent('li').siblings().children('a').removeClass('ms-active')
    $(this).addClass('ms-active')
})
/**
 * input 表单
 * <div> 错误信息
 * <div> 正确信息
 */
// 表单，显示正确
function input_show_true($input){
    $input.removeClass('is-invalid').addClass('is-valid')
}
// 表单，显示错误
function input_show_false($input, info){
    $input.next().next().html(info)
    $input.removeClass('is-valid').addClass('is-invalid')
}
function dateFormat(fmt, date) {
    let ret;
    const opt = {
        "Y+": date.getFullYear().toString(),        // 年
        "m+": (date.getMonth() + 1).toString(),     // 月
        "d+": date.getDate().toString(),            // 日
        "H+": date.getHours().toString(),           // 时
        "M+": date.getMinutes().toString(),         // 分
        "S+": date.getSeconds().toString()          // 秒
        // 有其他格式化字符需求可以继续添加，必须转化成字符串
    };
    for (let k in opt) {
        ret = new RegExp("(" + k + ")").exec(fmt);
        if (ret) {
            fmt = fmt.replace(ret[1], (ret[1].length == 1) ? (opt[k]) : (opt[k].padStart(ret[1].length, "0")))
        }
    }
    return fmt;
}

///////////////////// 渲染模态框的表单
// 根据打开模态框的按钮进行渲染
function renderModal(btn, data){
    let $model = $($(btn).data('bs-target'))
    for (let key in data) {
        $model.find("input[name='" +key+ "']").val(data[key])
        $model.find("textarea[name='" +key+ "']").val(data[key])
    }
}

function renderModalWithPrefix(btn, data, prefix){
    let $model = $($(btn).data('bs-target'))
    for (let key in data) {
        $model.find("input[name='" + prefix +key+ "']").val(data[key])
        $model.find("textarea[name='" + prefix + key+ "']").val(data[key])
    }
}
function clearModal(btn){
    let $model = $($(btn).data('bs-target'))
    $model.find('input').val('')
    $model.find('select').val('')
    $model.find('textarea').val('')
}