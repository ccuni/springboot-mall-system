// 调整弹窗的位置 : 确保弹窗能在当前页面显示
function fixToastPosition(){
    $('.ms-toast').css('top', $(document).scrollTop() + 80)
}
function ms_show_toast(isSuccess, info){
    fixToastPosition()
    if(isSuccess === true){
        let $x = $('#ms-toast-success')
        let $x_info = $('#ms-toast-success-info')
        $x.attr('data-bs-delay', DELAY_TOAST_SUCCESS)
        $x_info.text(info)
        $x.toast('show')
    } else{
        let $y = $('#ms-toast-fail')
        let $y_info = $('#ms-toast-fail-info')
        $y.attr('data-bs-delay', DELAY_TOAST_FAIL)
        $y_info.text(info)
        $y.toast('show')
    }
}
function ms_show_response(result, callback){
    if(result['code'] === CODE_SUCCESS) {
        ms_show_toast(true, result['msg'])
        callback()
    }
    else {
        ms_show_toast(false, result['msg'] || '权限不足')
    }
}
function ms_show_toast_callback(isSuccess, info, callback){
    ms_show_toast(isSuccess, info)
    setTimeout(function (){
        callback()
    }, isSuccess === true ?
                DELAY_TOAST_SUCCESS + DELAY_TOAST_WHEN_BTN:
                DELAY_TOAST_FAIL + DELAY_TOAST_WHEN_BTN)
}

function ms_show_toast_reload(isSuccess, info){
    ms_show_toast_callback(isSuccess, info, function (){
        location.reload()
    })
}