jQuery(function($){
    /**生成一个随机数**/
    function randomNum(min, max) {
        return Math.floor(Math.random() * (max - min) + min);
    }
    /**生成一个随机色**/
    function randomColor(min, max) {
        let r = randomNum(min, max);
        let g = randomNum(min, max);
        let b= randomNum(min, max);
        return "rgb(" + r + "," + g + "," + b + ")";
    }
    captcha_trueth=drawPic();
    document.getElementById("ms-captcha-changeImg").onclick = function(e) {
        e.preventDefault();
        captcha_trueth=drawPic();
    }
    /**绘制验证码图片**/
    function drawPic() {
        let canvas = document.getElementById("ms-captcha-canvas");
        let width = canvas.width;
        let height = canvas.height;
        //获取该canvas的2D绘图环境
        let ctx = canvas.getContext('2d');
        ctx.textBaseline ='bottom';
        /**绘制背景色**/
        ctx.fillStyle = randomColor(180,240);
        //颜色若太深可能导致看不清
        ctx.fillRect(0,0,width,height);
        /**绘制文字**/
        let str ='ABCEFGHJKLMNPQRSTWXYZ123456789';
        let code="";
        //生成四个验证码
        for(let i=1;i<=4;i++) {
            let txt = str[randomNum(0,str.length)];
            code=code+txt;
            ctx.fillStyle = randomColor(50,160);
            //随机生成字体颜色
            ctx.font = randomNum(15,40) +'px SimHei';
            //随机生成字体大小
            let x =10 +i *25;
            let y = randomNum(25,35);
            let deg = randomNum(-45,45);
            //修改坐标原点和旋转角度
            ctx.translate(x, y);
            ctx.rotate(deg * Math.PI /180);
            ctx.fillText(txt,0,0);
            //恢复坐标原点和旋转角度
            ctx.rotate(-deg * Math.PI /180);
            ctx.translate(-x, -y);
        }

        /**绘制干扰线**/
        for(let i=0;i< CAPTCHA_NUM_INTERFERE_LINE;i++) {
            ctx.strokeStyle = randomColor(40, 180);
            ctx.beginPath();
            ctx.moveTo(randomNum(0,width/2), randomNum(0,height/2));
            ctx.lineTo(randomNum(0,width/2), randomNum(0,height));
            ctx.stroke();
        }
        /**绘制干扰点**/
        for(let i=0;i < CAPTCHA_NUM_INTERFERE_POINT;i++) {
            ctx.fillStyle = randomColor(255);
            ctx.beginPath();
            ctx.arc(randomNum(0, width), randomNum(0, height),1,0,2* Math.PI);
            ctx.fill();
        }
        return code;
    }
});