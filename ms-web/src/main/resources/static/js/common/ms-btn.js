// 根据按钮的ID 来禁止按钮状态
function ms_btn_disable(btn_id, info) {
    $btn = $('#' + btn_id)
    let text = $btn.html()
    if (info != '') text = info
    $btn.html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>' + text)
    // // 禁止点击
    $btn.attr('disabled', true)
}
// 恢复状态
// 根据按钮的ID， 恢复按钮的状态
function ms_btn_enable(btn_id, info) {
    $btn = $('#' + btn_id)
    $btn.children('span').remove()
    $btn.attr('disabled', false)
    if(info != '')
        $btn.html(info)
}
// 定义当前正在倒计时的任务
var func
// 定义当前正在倒计时的按钮
var $btn_in
// 验证码禁止指定时间
function ms_btn_code_wait(btnId) {
    $btn_in = $('#' + btnId)
    let i = BTN_LAST_TIME / 1000
    // 定义每1秒执行1次的定时函数
    func = setInterval(function (){
        $btn_in.attr('disabled', true)
        $btn_in.text('重新发送: ' + i + 's')
        i--
        if(i <= 0) {                        // 验证码有效期过了，提示可重新获取验证码
           ms_btn_code_no_wait('重新发送验证码')
        }
    }, 1000)
}
// 恢复正在加载的按钮
function ms_btn_code_no_wait(text){
    $btn_in.attr('disabled', false)
    $btn_in.html('')
    $btn_in.html(text)
    clearInterval(func)
}