function ajax(reqType, url){
    let data = {}
    $.ajax({
        url: url,
        type: reqType,
        async: false,
        dataType: 'json',
        error: (err) =>{
            console.log('没有权限访问请求的地址: ' + url) 
            console.error(err.responseText)
        },
        success: (result) => { data = result }
    })
    return data
}

function ajaxByJSON(map){
    let data = {}
    $.ajax({
        url: map['url'],
        type: map['type'],
        async: false,
        data: map['data'],
        contentType: 'application/json;charset=utf-8',
        dataType: 'json',
        error: (err) =>{
            console.log(map['url']+'的AJAX请求失败, 异常:' + err.responseText)
            console.error(err.responseText)
        },
        success: (result) => { data = result }
    })
    return data
}
// 提交删除的AJAX，并返回结果
function ajaxDelete(param){
    let r = {code: '500'}
    $.ajax({
        url: param['url'],
        type: 'DELETE',
        data : param['data'],
        dataType: 'JSON',
        contentType: 'application/json;charset=utf-8',
        async: false,
        error: (err) =>{
            console.log(param['url']+'的AJAX请求失败, 异常:' + err.responseText)
            console.error(err.responseText)
        },
        success: (result) => {
            r = result
        }
    })
    return r
}
// 提交自定义的AJAX
function ajaxCustom(type, param){
    let r = {code:'500'}
    $.ajax({
        url: param['url'],
        type: type,
        data : param['data'],
        dataType: 'JSON',
        async: false,
        error: (err) =>{
            console.log(param['url']+'的AJAX请求失败, 异常:' + err.responseText)
            console.error(err.responseText)
        },
        success: (result) => {
            r = result
        }
    })
    return r
}
// 提交修改的 AJAX，并返回修改结果
function ajaxPostUpdate(param){
    let r = {code:'500'}
    $.ajax({
        url: param['url'],
        type: 'POST',
        data : param['data'],
        dataType: 'JSON',
        async: false,
        error: (err) =>{
            console.log(param['url']+'的AJAX请求失败, 异常:' + err.responseText)
            console.error(err.responseText)
        },
        success: (result) => {
            r = result
        }
    })
    return r
}
function ajaxPostJSON(url){
    let data = {}
    $.ajax({
        url: url,
        type: 'POST',
        async: false,
        dataType: 'json',
        error: (err) =>{
            console.log('没有权限访问请求的地址: ' + url) 
            console.error(err.responseText)
        },
        success: (result) => {
            if(result['code'] === CODE_SUCCESS)
                data = result['data']
        }
    })
    return data
}
function ajaxPostJSONByMap(url, map){
    let data = {}
    $.ajax({
        url: url,
        type: 'POST',
        data: map,
        async: false,
        dataType: 'json',
        error: (err) =>{
            console.log('没有权限访问请求的地址: ' + url) 
            console.error(err.responseText)
        },
        success: (result) => {
            data = result
        }
    })
    return data
}
function ajaxDeleteJSONByMap(url, map){
    let data = {}
    $.ajax({
        url: url,
        type: 'POST',
        data: map,
        async: false,
        dataType: 'json',
        error: (err) =>{
            console.log('没有权限访问请求的地址: ' + url) 
            console.error(err.responseText)
        },
        success: (result) => {
            data = result
        }
    })
    return data
}
function ajaxPostJsonByCurrentUrlParam(url){
    let data = {}
    $.ajax({
        url: url,
        type: 'POST',
        data:  paramGetURLtoJSON(),
        contentType: 'application/json;charset=utf-8',
        async: false,
        dataType: 'json',
        error: (err) =>{
            console.log('没有权限访问请求的地址: ' + url) 
            console.error(err.responseText)
        },
        success: (result) => {
            if(result['code'] === CODE_SUCCESS)
                data = result['data']
        }
    })
    return data
}