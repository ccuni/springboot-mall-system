let users = [
    {
        "address": "天津市-天津市-和平区-2341qwe123",
        "birthday": 1652889600000,
        "email": "862995359@qq.com",
        "gender": 1,
        "id": 1,
        "img": "https://uni1024.oss-cn-hangzhou.aliyuncs.com/mall-system/images/users/woaini/2022-05-221653211267494.jpeg",
        "password": "123456",
        "role": {
            "id": 4,
            "name": "管理员"
        },
        "tel": "13311111111",
        "usercode": "woaini",
        "username": "woaini"
    }
]
let page = {
    "current": 1,
    "optimizeCountSql": true,
    "orders": [],
    "pages": 1,
    "records": [
        {
            "address": "天津市-天津市-和平区-2341qwe123",
            "birthday": 1652889600000,
            "email": "862995359@qq.com",
            "gender": 1,
            "id": 1,
            "img": "https://uni1024.oss-cn-hangzhou.aliyuncs.com/mall-system/images/users/woaini/2022-05-221653211267494.jpeg",
            "password": "123456",
            "role": {
                "id": 4,
                "name": "管理员"
            },
            "tel": "13311111111",
            "usercode": "woaini",
            "username": "woaini"
        },
    ],
    "searchCount": true,
    "size": 5,
    "total": 1
}
let roleType = [
    {
        "id":1,
        "name":"游客"
    },
    {
        "id":2,
        "name":"普通用户"
    },
]
let $entry_userroles = $('#admin-card-user-list-entry')
let $entry_pages=$('#admin-card-user-pages-entry')
let $entry_role_dropdown=$('#admin-user-role-dropdown-entry')
let $preload = $('#admin-user-center-placeholder')
function renderUsers(users, delay, callback){
    console.log(users)
    $entry_userroles.html('')
    setTimeout(function (){
        callback()
        users.forEach(function (user, index){
            let fn_modify = "renderModal(" + user['id'] + ")"
            let fn_remove = "removeUser(" + user['id'] + ")"
            $entry_userroles.append(`
            <tr valign="baseline">
                <td><input type="checkbox" class="cb_user" data-rid="${user['role']['id']}" data-code="${user['usercode']}" data-uid=${user['id']} style="width: 20px; height: 20px;"></td>
                <td>${user['id']}</td>
                <td>${user['usercode']}</td>
                <td>${user['username']}</td>
                <td>${user['gender'] === 1 ? '男' : '女'}</td>
                <td><img src="${user['img']}" width="80px" height="80px" style="border-radius: 50%;"/></td>
                <td>${user['tel']}</td>
                <td>${user['address']}</td>
                <td>
                     <button class="btn btn-primary w-25" data-bs-toggle="modal" data-bs-target="#modal-base" onclick="${fn_modify}">编辑</button>
                     <button class="btn btn-danger w-25" onclick="${fn_remove}">删除</button>
                </td>
          </tr>
        `)
        })
    }, delay)
}
///// 跳转到指定页
function toPage(inputId, mini, maxi){
    $input = $('#' + inputId)
    let page = $input.val()
    // 限制页面的范围
    page = Math.min(page, maxi)
    page = Math.max(page, mini)
    location.href = paramUpdateCurrentParam('pageNum', page)
}

// 加载分页
function renderRolePage(page){
    $entry_pages.html('')
    let $perPage = $(`<li></li>`)
    // 渲染尾部
    let $tail = `<li class="align-self-center r">
                &nbsp;&nbsp;到第&nbsp;&nbsp;
            </li>
            <li class="align-self-center">
                <input id="admin-user-input-to-page" class="input-group" name="pageNum" size="5">
            </li>
            <li class="align-self-center">
                &nbsp;页&nbsp;
            </li>
            <li class="align-self-center">
                <button class="btn btn-outline-dark" onclick="toPage('admin-user-input-to-page', 1, page.pages)">确定</button>
            </li>`
    // 渲染每一页
    for (let i = page.startPage; i <= page.endPage; i++) {
        if(i === page.current){ // 渲染当前页，设置一个高亮的效果
            $perPage.append(`
            <li class="page-item mx-2 active" aria-current="page">
             <a class="page-link d-block" href="${paramUpdateCurrentParam('pageNum', i)}">${i}</a>
         </li>`)
        } else {
            $perPage.append(`
             <li class="page-item mx-2" aria-current="page">
                 <a class="page-link d-block" href="${paramUpdateCurrentParam('pageNum', i)}">${i}</a>
             </li>`
            )
        }
    }
    $entry_pages.html('')
    $entry_pages.html(`
            <ul class="pagination mx-auto justify-content-center">
            <li class="align-self-center me-3 rounded-3">
                    第 ${page.current} 页 &nbsp;&nbsp; 共 ${page.pages} 页
            </li>
            <li class="page-item ms-page">
                 <a class="page-link" href="${paramUpdateCurrentParam('pageNum', 1)}">
                    首页
                 </a>
            </li>
            <!--上一页-->
           <li class="page-item ms-page">
                <a class="page-link" href="${paramUpdateCurrentParam('pageNum', page.prePage)}">上一页</a>
            </li>`
        + $perPage.html() + `
            <!--下一页-->
             <li class="page-item ms-page">
                <a class="page-link" href="${paramUpdateCurrentParam('pageNum', page.nextPage)}">下一页</a>
            </li>
            <!--最后一页-->
             <li class="page-item ms-page" href="/index.html?pageNum=${page.pages}">
                  <a class="page-link" href="${paramUpdateCurrentParam('pageNum', page.pages)}">末页</a>
             </li>` + $tail +
        `</ul>`)
    $('#textShowResult').text('共 ' + page.total + ' 条结果')

}
// 渲染角色的下拉菜单
function renderRoleDropdown(roleType){
    $entry_role_dropdown.html('')
    roleType.forEach(function (role){
        $entry_role_dropdown.append(`
              <li><a class="dropdown-item" href="${paramUpdateCurrentParams(['typeId', 'pageNum'], [role.id, 1])}">${role.name}</a></li>
        `)
    })
    $entry_role_dropdown.prepend(`<li><a class="dropdown-item" href="${paramDeleteCurrentURLNot('typeId')}"</a>全部角色</li>`)
}
// 编辑某个用户时渲染修改的模态框
function renderModal(user_id){
    let user = ajaxPostJSON('/user/select/' + user_id)[0]
    let birth = user['birthday'].split('T')[0].split('-')
    $("#ms-img-head").attr('src', user['img'])
    $("input[name='id']").val(user['id'])
    $("input[name='usercode']").val(user['usercode'])
    $("input[name='username']").val(user['username'])
    $("input[name='email']").val(user['email'])
    if(user['gender'] == '1')
        $('#ms-form-user-base-gender-1').prop('checked', true)
    else
        $('#ms-form-user-base-gender-0').prop('checked', true)
    $("input[name='tel']").val(user['tel'])
    $("input[name='birthday']").val(birth[0] + '-' + birth[1] + '-' + birth[2])
    // 省市区三级联动调用 AJAX查询后渲染
    renderAddress(ajaxPostAddressByUserCode(user['usercode']))
    // 单独渲染详细地址
    let i = user['address'].lastIndexOf('-')
    $("input[name='address']").val(user['address'].substring(i+1))
}
// 编辑选中用户的角色时，渲染修改的模态框， 数据从选择框所选中的标签里获取
function renderModalBySelected(){
    userCodes  = []
    role = []
    $('.cb_role').each(function (index, obj){
        if($(this).prop('checked') === true){
            // 根据HTML元素的相对关系，定位到用户账号
            userCodes.push($(this).parent().next().text())
            // 同上，定位到当前用户的角色
            role.push($(this).data('rid'))
        }
    })
    if(role.length == 0 || userCodes.length != role.length){
        ms_show_toast(false, '请选择需要处理的角色')
    }
    // 将用户账号以逗号连接起来，角色则默认选择第一个用户的角色
    let code = ''
    for(let i = 0; i < userCodes.length; i++){
        if(i === 0)
            code = userCodes[i]
        else
            code = code + ',' + userCodes[i]
    }
    renderModal(code, role[0])
}

// 新增用户时
function renderModalInsert(btn){
    clearModal(btn)
}
function ajaxGetAllUserWithRole(){
    let data = ajaxPostJsonByCurrentUrlParam('/role/select')
    // 计算上一页和下一页
    let prePage = Math.max(1, data['current'] - 1)
    let nextPage = Math.min(data['current'] + 1, data['pages'])
    let startPage = Math.max(prePage - 1, 1)
    let endPage = Math.min(nextPage + 1, data['pages'])
    data['prePage'] = prePage
    data['nextPage'] = nextPage
    data['startPage'] = startPage
    data['endPage'] = endPage
    return data
}
// 停止加载
function ajaxGetAllRoleType() {return ajaxPostJSON('/role/selectall')}

function stopLoadUI(){
    $preload.html('')
}
// 开启加载
function startLoadUI(){
    $preload.html(`
     <div class="spinner-border text-center" role="status">
        <span class="visually-hidden">Loading...</span>
    </div>
    `)
}
$('#btn-search-role').click(function (){
    $input = $(this).siblings('input')
    if($input.val().length > 0)
        location.href = paramUpdateCurrentParam('username', $input.val())
    else
        location.href = paramDeleteCurrentURLNot('username')
})
startLoadUI()
page = ajaxGetAllUserWithRole()
roleType = ajaxGetAllRoleType()

// 删除用户
function removeUser(userId){
    if(!confirm('请问确认要删除该用户吗？'))
        return
    let result = ajaxCustom('DELETE',
        {'url': '/user/delete/' + userId}
    )
    if(result['code'] === CODE_SUCCESS){
        ms_show_toast_reload(true, result['msg'])
    } else
        ms_show_toast(false, '权限不足')
}
// 删除多个用户
function removeUsers(){
    let ids = []
    // 获取选择的需要删除的供应商
    $('.cb_user').each(function (){
        if($(this).prop('checked') === true){
            ids.push($(this).data('uid'))
        }
    })
    if(ids.length === 0) { ms_show_toast(false, '请选择需要删除的用户') ;return }
    if(!confirm('确认要删除这些吗？')) return
    let result =  ajaxDelete({
        url: '/user/delete/' + ids,
        data: ''
    })
    if(result['code'] === CODE_SUCCESS){
        ms_show_toast_reload(true, result['msg'])
    } else
        ms_show_toast(false, '权限不足')
}
//////////////////////////////////////////////////// 表单验证 开始

// 基本信息的表单
$form_user_base = $('#ms-form-user-base')
// 基本信息表单的输入框
$base_username = $('#ms-form-user-base-username')
var flag_username = false
$base_tel = $('#ms-form-user-base-tel')
var flag_tel = false
$base_birthday = $('#ms-form-user-base-birthday')
var flag_birthday = false
$base_address = $('#ms-form-user-base-address')
var flag_address = false
$base_email = $('#ms-form-user-base-email')
var flag_email = false
$base_email.blur(function (){
    let email = $(this).val()
    $errorText = $('#ms-form-user-base-email-fail')
    let errorText = ''
    let reg = /^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/g
    flag_email = true
    if(email.length === 0){
        flag_email = false
        errorText = '提示: 邮箱不能为空'
    }
    else if(email.length > 30){
        flag_email = false
        errorText = '提示: 邮箱的长度不能超过30'
    }
    else if(!reg.test($base_email.val())) {
        flag_email = false
        errorText = '提示: 输入的邮箱必须符合xxx@xxx.xxx的格式，且不超过30个字符'
    }
    if (flag_email === false){
        $errorText.html(errorText)
        $base_email.removeClass('is-valid').addClass('is-invalid')
    } else
        $base_email.removeClass('is-invalid').addClass('is-valid')
})
// 用户名验证
$base_username.blur(function (){
    $errorText = $('#ms-form-user-base-username-fail')   // 错误提示
    let errorText = ''
    let value = $base_username.val()
    let reg = /^[0-9a-zA-Z\u4e00-\u9fa5]{2,11}$/
    flag_username = true
    // 1. 不能为空
    if(value === '') {
        flag_username = false
        errorText = '提示: 用户名不能为空!'
    }
    // 2. 字符长不能超过10
    else if (value.length < 2 || value.length >= 10) {
        flag_username = false
        errorText = '提示: 用户名长度不能超过10且不能的低于2'
    }
    // 3. 只能是数字、字母、下划线，不能包含特殊符号
    else if(reg.test(value) === false) {
        flag_username = false
        errorText = '提示: 用户名只能是汉字、数字或字母，不能包含特殊符号'
    }
    if(flag_username === false) {
        $base_username.removeClass('is-valid').addClass('is-invalid')
        $errorText.html(errorText)
    }
    else {
        $base_username.removeClass('is-invalid').addClass('is-valid')
    }
})
// 联系方式验证
$base_tel.blur(function (){
    $errorText = $('#ms-form-user-base-tel-fail')
    let reg = /^1[3-9][0-9]{9}$/g
    let value = $base_tel.val()
    let errorText = ''
    flag_tel = true
    if(value == ''){
        flag_tel = false
        errorText = '提示: 联系方式不能为空'
    } else if(!reg.test(value)) {
        flag_tel = false
        errorText = '提示: 输入的联系方式必须为13~19开头的11位数字'
    }
    if(flag_tel === false){
        $errorText.html(errorText)
        $base_tel.removeClass('is-valid').addClass('is-invalid')
    } else
        $base_tel.removeClass('is-invalid').addClass('is-valid')
})

// 生日验证
$base_birthday.blur(function (){
    flag_birthday = true
    if($base_birthday.val() == ''){
        flag_birthday = false
        $(this).removeClass('is-valid').addClass('is-invalid')
    } else
        $(this).removeClass('is-invalid').addClass('is-valid')
})

// 详细地区验证
$base_address.blur(function (){
    flag_address = true
    let $errorText = $('#ms-form-user-base-address-fail')
    let errorText = ''
    let value = $base_address.val()
    if(value == ''){
        flag_address = false
        errorText = '提示: 详细地区不能为空'
    } else if(value.length >= 128){
        flag_address = false
        errorText = '提示: 详细地区不能超过128个字符,目前已达到: ' + value.length
    }

    if(flag_address === false){
        $errorText.html(errorText)
        $(this).removeClass('is-valid').addClass('is-invalid')
    } else
        $(this).removeClass('is-invalid').addClass('is-valid')
})

//////////////////////////////////////////////////// 表单验证 结束

// 按键监听
$(document).keydown(function(event){
    if (event.keyCode === 13) {//回车键登陆
        $('#btn-search-role').click()
    }
});
////////////////// 全选
$('#cb_all').change(function (){
    $('.cb_role').prop('checked', $(this).prop('checked'))
})
//////////////////// 保存用户的修改
function checkBaseForm(){
    $base_username.blur()
    $base_tel.blur()
    $base_birthday.blur()
    $base_address.blur()
    $base_email.blur()

    return flag_username === true
        && flag_tel === true && flag_birthday === true && flag_address === true
        && flag_email
}
function checkInsertForm(){
    $form = $('#ms-form-user-insert')
    /////////////////////////// 验证 用户名
    let $userCode = $form.find("input[name='usercode']")   // 错误提示
    let $username = $form.find("input[name='username']")   // 密码提示
    let $password = $form.find("input[name='password']")   // 密码提示
    let $tel = $form.find("input[name='tel']")   // 联系方式
    let $birthday = $form.find("input[name='birthday']")   // 生日
    let $address = $form.find("input[name='address']")   // 地区
    let $email = $form.find("input[name='email']")  // 邮箱
    let flag = 7
    // 账号验证
    if(/^[0-9a-zA-Z\u4e00-\u9fa5]{2,11}$/.test($userCode.val()) === false) {
        input_show_false($userCode, '提示: 账号只能是汉字、数字或字母，不能包含特殊符号')
        flag --
    } else input_show_true($userCode)
    // 用户名验证
    if(/^[0-9a-zA-Z\u4e00-\u9fa5]{2,11}$/.test($username.val()) === false) {
        input_show_false($username, '提示: 用户名只能是汉字、数字或字母，不能包含特殊符号')
        flag --
    } else input_show_true($username)
    // 密码验证
    if($password.val() === '' || (!/^[0-9a-zA-Z]{6,11}$/.test($password.val()))) {
        input_show_false($password, '提示: 密码必须在6-11位，且只能包含数字或者字母')
        flag --
    } else input_show_true($password)
    // 联系方式验证
    if($tel.val() === '' || !/^1[3-9]\d{9}$/g.test($tel.val())) {
        input_show_false($tel, '提示: 输入的联系方式必须为13~19开头的11位数字')
        flag --
    } else
        input_show_true($tel)
    // 生日验证
    if(!/^\d{4}-\d{2}-\d{2}$/.test($birthday.val())){
        input_show_false($birthday, '提示: 生日不能为空')
        flag --
    } else
        input_show_true($birthday)
    //  地区验证
    if($address.val() === '' || !/.{3,128}$/g.test($address.val())) {
        input_show_false($address, '提示: 输入的地区长度必须在3~128的范围')
        flag --
    } else input_show_true($address)
    // 邮箱验证
    if($email.val() === '' || !/^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\.[a-zA-Z0-9_-]+)+$/g.test($email.val())) {
        input_show_false($email, '提示: 输入的邮箱必须符合xxx@xxx.xxx的格式，且不超过30个字符')
        flag --
    } else input_show_true($email)
    return flag === 7

}
// 修改用户信息时保存基本信息
function saveUserBase(btn_id){
    let check = checkBaseForm()
    // 检验表单
    if(check === false) {
        ms_show_toast(false, '请检查输入的信息是否符合要求.')
        return true;
    }
    ms_btn_disable(btn_id, '保存中')
    setTimeout(function (){
        $.ajax({
            url: '/user/update/' + $('#ms-form-user-base-id').val(),
            type: 'POST',
            data: $form_user_base.serialize(),
            dataType: 'json',
            error: function (err) {
                ms_show_toast(false, err.responseText)
                console.error(err.responseText)
                ms_btn_enable(btn_id, '保存')
            },
            success: function (result){
                if(result['code'] === CODE_SUCCESS){
                    ms_show_toast_callback(true, result['msg'], function (){
                        // 刷新页面
                        window.location.reload()
                    })
                } else {
                    ms_show_toast(false, result['msg'])
                }
                ms_btn_enable(btn_id, '保存')
            }
        })
    }, DELAY_TOAST_WHEN_BTN)
}
function saveUserInsert(btn_id){
    let check = checkInsertForm()
    // 检验表单
    if(check === false) {
        ms_show_toast(false, '请检查输入的信息是否符合要求.')
        return true;
    }
    ms_btn_disable(btn_id, '保存中')
    setTimeout(function (){
        $.ajax({
            url: '/user/insert',
            type: 'POST',
            data: $('#ms-form-user-insert').serialize(),
            dataType: 'json',
            error: function (err) {
                ms_show_toast(false, err.responseText)
                console.error(err.responseText)
                ms_btn_enable(btn_id, '重新保存')
            },
            success: function (result){
                if(result['code'] === CODE_SUCCESS){
                    ms_btn_enable(btn_id, '保存')
                    ms_show_toast_callback(true, result['msg'], function (){
                        // 刷新页面
                        window.location.reload()
                    })
                } else
                    ms_show_toast(false, result['msg'])
                ms_btn_enable(btn_id, '重新保存')
            }
        })
    }, DELAY_TOAST_WHEN_BTN)
}

////////////////////////////////////////////////// 编辑头像 START
/**
 * 向后台发送请求获取OSS的用户默认头像图片信息，
 * 返回值是一个 JSON格式的数组
 * [ {头像1名称, 头像1url}, {头像2名称, 头像2url}, ...]
 */
function getOssImages(){
    let imgs = []
    $.ajax({
        url: '/user/oss',
        async: false,
        type: 'POST',
        dataType: 'json',
        error: function (err){
            ms_show_toast(false, '获取OSS图像的AJAX请求失败')
            console.error(err.responseText)
        },
        success: function (result){
            if(result['code'] === CODE_SUCCESS){
                imgs = result['data']
            } else
                ms_show_toast(false, '未成功获取OSS图像')
        }
    })
    return imgs
}
/**
 * 显示默认头像的弹窗
 */
function showDefaultHeadModal(){
    chose_img = ''
    let imgs = getOssImages()
    $ul = $('#ms-modal-user-default-img ul')
    // 动态渲染当前 OSS 存在的头像
    $ul.html(``)
    $.each(imgs, (index, img)=>{
        if(img.length === 2) {
            let name = img[0]
            let url = img[1]
            // 一行五个 ，<tr> 表示换行
            $ul.append(`<li class="w-auto mt-3 mb-3"><img src="${url}" class="ms-img-small"></li>`)
        }
    })
    $ul.append(`</tbody>`)
    // 设置监听
    $('.ms-img-small').click(function (){
        // 先移除所有的动态
        $('.ms-img-small').removeClass('ms-img-choose')
        // 再启动当前点击的动态
        $(this).addClass('ms-img-choose')
    })
}

/**
 * 保存用户的头像
 */
function saveUserHead(btn_id){
    ms_btn_disable(btn_id, '保存中')
    setTimeout(function(){
        $.ajax({
            url: '/user/img/' + $("input[name='id']").val(),
            type: 'POST',
            data: {'imgUrl': $('.ms-img-choose').attr('src')},
            dataType: 'json',
            error: function (err) {
                ms_show_toast(false, '保存用户头像的AJAX请求失败')
                ms_btn_enable(btn_id, '保存')
                console.error(err.responseText)
            },
            success: function (result){
                if(result['code'] === CODE_SUCCESS){
                    ms_show_toast_callback(true, result['msg'], function (){
                        window.location.reload()
                    })
                }
                else ms_show_toast(false, result['msg'])
                ms_btn_enable(btn_id, '保存')
            }
        })
    }, DELAY_TOAST_WHEN_BTN)
}

///////////////////////////////////////////////// 编辑头像 END

$(document).ready(function (){
    useImgPlugin(400, 300, 150, 150);
    renderRoleDropdown(roleType)
    renderUsers(page['records'], DELAY_LOAD_USER_BASE, () => stopLoadUI())
    renderRolePage(page)
    // 自定义修改头像
    $('#modal-sure-custom').click(function (){ saveUserHeadById($(this).attr('id'), $("input[name='id']").val())})
})