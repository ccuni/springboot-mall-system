package com.star.ms.config;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import com.star.ms.admin.service.user.PermService;
import com.star.ms.admin.service.user.RoleService;
import com.star.ms.admin.shiro.CustomRealm;
import com.star.ms.common.entity.user.Perm;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


@Configuration
public class ShiroConfig {

    @Autowired
    RoleService roleService;
    @Autowired
    PermService permService;
    // 创建 Realm 对象
    @Bean
    public CustomRealm customerRealm(){
        CustomRealm realm = new CustomRealm();
        // 修改凭证校验匹配器
        HashedCredentialsMatcher hcm = new HashedCredentialsMatcher();
        hcm.setStoredCredentialsHexEncoded(true);
        // 设置加密算法为 MD5
        hcm.setHashAlgorithmName("MD5");
        // 设置散列次数
        hcm.setHashIterations(1024);
        realm.setCredentialsMatcher(hcm);
        return realm;
    }
    // 安全管理器
    @Bean(name="securityManager")
    public DefaultWebSecurityManager getDefaultWebSecurityManager(
            @Qualifier("customerRealm") Realm realm){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(realm);
        return securityManager;
    }
    // 过滤链工厂
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(
            @Qualifier("securityManager") DefaultWebSecurityManager defaultWebSecurityManager){
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        // 设置安全管理器
        bean.setSecurityManager(defaultWebSecurityManager);
        Map<String, String> filterMap = new LinkedHashMap<>();

        // 获取所有权限信息 配置数据库的权限信息
        List<Perm> perms = permService.list();
        perms.forEach(perm ->{
            for (String p : perm.getUrl().split(",")) {
                filterMap.put(p, String.format("perms[%s]",p));
            }
        });
        // 配置公共权限 (游客的权限)
        filterMap.put("/user/login", "anon");
        filterMap.put("/user/verify", "anon");
        filterMap.put("/register/*", "anon");
        filterMap.put("/login.html", "anon");
        filterMap.put("/home.html", "anon");
        bean.setFilterChainDefinitionMap(filterMap);
        // 设置登录的请求
        bean.setLoginUrl("/login.html");
        // 未授权页面
        bean.setUnauthorizedUrl("/noauth");
        return bean;
    }
    // 整合 ShiroDialect: 用来整合 shiro thymeleaf
    @Bean
    public ShiroDialect getShiroDialect(){
        return new ShiroDialect();
    }
    // Shiro注解开发需要的Bean
    @Bean
    public AuthorizationAttributeSourceAdvisor getAuthorizationAttributeSourceAdvisor(){
        return new AuthorizationAttributeSourceAdvisor();
    }
}

