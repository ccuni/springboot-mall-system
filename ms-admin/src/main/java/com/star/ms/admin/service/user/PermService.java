package com.star.ms.admin.service.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.entity.user.Perm;

import java.util.List;
import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_perm】的数据库操作Service
* @createDate 2022-05-22 13:28:53
*/
public interface PermService extends IService<Perm> {

    List<Perm> getByRoleId(Integer id);

    IPage<Perm> getPermsPageByMap(Map<String, Object> condition);

    boolean addPerm(String name, String url);

    boolean removePerms(List<Integer> permIds);
}
