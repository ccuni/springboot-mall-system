package com.star.ms.admin.service.impl.address;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.star.ms.common.dao.mapper.address.AreaMapper;
import com.star.ms.common.entity.address.Area;
import com.star.ms.admin.service.address.AreaService;
import org.springframework.stereotype.Service;

/**
* @author uni10
* @description 针对表【ms_area】的数据库操作Service实现
* @createDate 2022-05-22 13:28:21
*/
@Service
public class AreaServiceImpl extends ServiceImpl<AreaMapper, Area>
    implements AreaService {

}




