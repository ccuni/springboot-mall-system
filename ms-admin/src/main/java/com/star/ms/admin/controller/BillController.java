package com.star.ms.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.star.ms.admin.service.bill.BillService;
import com.star.ms.common.entity.Bill;
import com.star.ms.common.entity.RestResponse;
import com.star.ms.common.entity.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/bill")
public class BillController {
    @Autowired
    private BillService billService;


    /**
     * 查询商品信息
     */
    @PostMapping("/query")
    public RestResponse<IPage<Bill>> getBill(@RequestBody(required = false) Map<String, Object> condition){
        int pageNum = 1;
        int pageSize = 5;
        // 处理空参
        if(condition != null){
            pageNum = Integer.parseInt(condition.getOrDefault("pageNum", 1).toString());
            pageSize = Integer.parseInt(condition.getOrDefault("pageSize", 5).toString());
        }
        IPage<Bill> page = billService.getBillPageByCondition(pageNum, pageSize, condition);
        return RestResponse.success(page);
    }

    @PostMapping("/update/status/{billIds}")
    public RestResponse<String> updateBillStatus(@PathVariable List<Long> billIds, @RequestParam Integer status){
        return billService.updateStatus(billIds, status) ?
                RestResponse.success("修改成功!")
                    : RestResponse.fail("修改失败");
    }
    @DeleteMapping("/delete/{billIds}")
    public RestResponse<String> deleteBills(@PathVariable List<Long> billIds){
        return billService.removeBatchByIds(billIds) ?
                RestResponse.success("删除成功!")
                    :RestResponse.fail("删除失败");
    }
    @PostMapping("/query/{billId}")
    public RestResponse<Bill> queryBill(@PathVariable Long billId){
        return RestResponse.success(billService.getBillWithAllById(billId));
    }
    @PostMapping("/insert")
    public RestResponse<Bill> addBill(@RequestBody Map<String, Object> map,
                                       HttpSession session){
        Bill bill = billService.addBillByMap(map);
        return bill != null ?
                RestResponse.success(bill) : RestResponse.fail(null);
    }


    @PostMapping("/update/pay/{billId}")
    public RestResponse<String> payBill(@PathVariable Long billId){
        return billService.payBillById(billId) ?
                RestResponse.success("账单结算成功!")
                    : RestResponse.fail("账单结算失败.");
    }
    @PostMapping("/update/cancel/{billId}")
    public RestResponse<String> cancelBill(@PathVariable Long billId){
        return billService.cancelBillById(billId) ?
                RestResponse.success("订单已取消!")
                    : RestResponse.fail("订单取消失败，请联系管理员");
    }

    /**
     * 查看最近的几条订单
     * @param num
     * @return
     */
    @PostMapping("/select/current/{num}")
    public RestResponse<List<Bill>> getCurrentBills(@PathVariable Integer num){
        return RestResponse.success(billService.getCurrentBills(num));
    }
    @PostMapping("/selectCountMoney")
    public RestResponse<Double> countBillsMoney(){
        return RestResponse.success(billService.countMoney());
    }
    @PostMapping("/selectMoneyDetail")
    public RestResponse<Map<String, Object>> getMoneyByCategory(){
        return RestResponse.success(billService.countMoneyByCategory());
    }
}
