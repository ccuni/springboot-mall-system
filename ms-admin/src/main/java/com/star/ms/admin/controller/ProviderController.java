package com.star.ms.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.star.ms.admin.service.provider.ProviderService;
import com.star.ms.common.entity.RestResponse;
import com.star.ms.common.entity.provider.Provider;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/provider")
public class ProviderController {
    @Autowired
    private ProviderService providerService;

    /**
     * 查询商品信息
     * @param condition 查询条件
     * @return
     */
    @PostMapping("/query")
    public RestResponse<IPage<Provider>> getProduct(@RequestBody(required = false) Map<String, Object> condition){
        int pageNum = 1;
        int pageSize = 5;
        // 处理空参
        if(condition != null){
            pageNum = Integer.parseInt(condition.getOrDefault("pageNum", 1).toString());
            pageSize = Integer.parseInt(condition.getOrDefault("pageSize", 5).toString());
        }
        IPage<Provider> page = providerService.getProviderPageWithTypeBySome(pageNum, pageSize, condition);
        return RestResponse.success(page);
    }
    @PostMapping("/query/{id}")
    public RestResponse<Provider> getProviderOne(@PathVariable Long id){
        return RestResponse.success(providerService.getOneById(id));
    }
    @PostMapping("/update")
    public RestResponse<String> updateProvider(Provider provider){
        return providerService.updateById(provider) ? RestResponse.success("修改成功!") : RestResponse.fail("修改失败");
    }

    @DeleteMapping("/delete/{ids}")
    public RestResponse<String> deleteProvider(@PathVariable final List<Long> ids){
        return providerService.removeByIds(ids) ?
                RestResponse.success("删除成功!") :
                    RestResponse.fail("删除失败");
    }
    @PostMapping("/insert")
    public RestResponse<String> insertProvider(Provider provider){
        System.out.println("新添加的供应商: " + provider);
        return providerService.save(provider) ?
                RestResponse.success("添加成功!"):
                    RestResponse.fail("添加失败.");
    }
}
