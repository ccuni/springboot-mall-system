package com.star.ms.admin.service.address;

import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.entity.address.Province;

/**
* @author uni10
* @description 针对表【ms_province】的数据库操作Service
* @createDate 2022-05-22 13:29:15
*/
public interface ProvinceService extends IService<Province> {

}
