package com.star.ms.admin.service.impl.user;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.star.ms.common.dao.mapper.user.UrMapper;
import com.star.ms.common.entity.user.Ur;
import com.star.ms.admin.service.user.UrService;
import org.springframework.stereotype.Service;

/**
* @author uni10
* @description 针对表【ms_ur】的数据库操作Service实现
* @createDate 2022-05-22 13:29:33
*/
@Service
public class UrServiceImpl extends ServiceImpl<UrMapper, Ur>
    implements UrService {

}




