package com.star.ms.admin.service.impl.user;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.star.ms.common.dao.mapper.user.RpMapper;
import com.star.ms.common.entity.user.Rp;
import com.star.ms.admin.service.user.RpService;
import org.springframework.stereotype.Service;

/**
* @author uni10
* @description 针对表【ms_rp】的数据库操作Service实现
* @createDate 2022-05-22 13:29:27
*/
@Service
public class RpServiceImpl extends ServiceImpl<RpMapper, Rp>
    implements RpService {

}




