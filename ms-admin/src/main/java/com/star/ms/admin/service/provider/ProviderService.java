package com.star.ms.admin.service.provider;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.entity.provider.Provider;

import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_provider】的数据库操作Service
* @createDate 2022-05-22 13:29:09
*/
public interface ProviderService extends IService<Provider> {

    IPage<Provider> getProviderPageWithTypeBySome(int pageNum, int pageSize, Map<String, Object> condition);

    Provider getOneById(Long id);
}
