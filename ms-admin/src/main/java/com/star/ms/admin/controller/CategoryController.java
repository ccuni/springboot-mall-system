package com.star.ms.admin.controller;

import com.star.ms.admin.service.product.CategoryService;
import com.star.ms.common.entity.RestResponse;
import com.star.ms.common.entity.product.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;
    /**
     * 查询商品的类型
     * @return
     */
    @PostMapping("/query")
    public RestResponse<List<Category>> queryAllCategoryWithCount(){
        List<Category> data = categoryService.getAllCategory();
        return RestResponse.success(data);
    }
    @PostMapping("/queryCount")
    public RestResponse<Map<String, Object>> queryCategoryWithCount(){
        List<Category> list = categoryService.getAllCategory();
        List<String> name = list.stream().map(Category::getName).collect(Collectors.toList());
        List<Long> value = list.stream().map(Category::getSum).collect(Collectors.toList());
        return RestResponse.success(new HashMap<String, Object>(){{
            put("productName", name);
            put("productCount", value);
        }});
    }
}
