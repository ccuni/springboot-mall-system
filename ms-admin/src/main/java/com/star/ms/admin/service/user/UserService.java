package com.star.ms.admin.service.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.vo.UserAddressCodeVo;
import com.star.ms.common.entity.product.Product;
import com.star.ms.common.entity.user.User;
import com.star.ms.common.vo.UserProvinceCountVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_user】的数据库操作Service
* @createDate 2022-05-22 13:29:39
*/
@Service
public interface UserService extends IService<User> {

    boolean verifyEmailCode(User user, String emailCode);

    User register(User user);
    User insertUser(User user, UserAddressCodeVo addressCodeVo);

    boolean sendRegisterEmail(User user);
    public boolean sendRetrieveEmail(User user);
    User login(User user);

    boolean verifyCodeExisted(User user);

    int saveImgByCode(String usercode, String imgUrl);

    boolean savePassword(User user, boolean needLogout);

    boolean saveEmail(User user);

    List<User> getListWithRole(Map<String, Object> condition);

    IPage<Product> getListWithRoleBySome(Map<String, Object> param);

    List<User> queryWithRoleByIds(List<Long> ids);

    boolean saveBaseById(User user, UserAddressCodeVo addressVo);

    int saveImgById(Long userId, String imgUrl);

    boolean saveUsersWithRole(List<User> randomUser);

    int getMaxId();

    User getListById(Long reviewsUserId);

    List<UserProvinceCountVo> getListWithProvinceCount();

    User getByCode(String userCode);

    boolean retrieve(User user, String emailCode);

    boolean hasExisted(String email);
}
