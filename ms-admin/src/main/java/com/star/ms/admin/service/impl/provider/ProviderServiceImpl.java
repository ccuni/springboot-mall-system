package com.star.ms.admin.service.impl.provider;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.star.ms.admin.dao.impl.provider.ProviderDAOImpl;
import com.star.ms.common.dao.mapper.provider.ProviderMapper;
import com.star.ms.common.entity.provider.Provider;
import com.star.ms.admin.service.provider.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_provider】的数据库操作Service实现
* @createDate 2022-05-22 13:29:09
*/
@Service
public class ProviderServiceImpl extends ServiceImpl<ProviderMapper, Provider>
    implements ProviderService {
    @Autowired
    private ProviderDAOImpl providerDAO;

    @Override
    public IPage<Provider> getProviderPageWithTypeBySome(int pageNum, int pageSize, Map<String, Object> condition) {
        return providerDAO.selectProductPageByMapWithType(new Page<>(pageNum, pageSize), condition);
    }

    @Override
    public Provider getOneById(Long id) {
        return providerDAO.selectOneById(id);
    }
}




