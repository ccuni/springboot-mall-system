package com.star.ms.admin.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.star.ms.admin.service.user.PermService;
import com.star.ms.common.entity.RestResponse;
import com.star.ms.common.entity.user.Perm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/perm")
public class PermController {
    @Autowired
    private PermService permService;
    @PostMapping("/select")
    public RestResponse<IPage<Perm>> getPerms(@RequestBody(required = false) Map<String, Object> condition){
        return RestResponse.success(permService.getPermsPageByMap(condition));
    }
    @PostMapping("/select/{permId}")
    public RestResponse<Perm> getPerm(@PathVariable Integer permId){
        LambdaQueryWrapper<Perm> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Perm::getId, permId);
        return RestResponse.success(permService.getOne(wrapper));
    }

    @PostMapping("/insert")
    public RestResponse<IPage<Perm>> getPerms(@RequestParam String name, @RequestParam String url){
        return permService.addPerm(name, url) ?
                RestResponse.success("添加成功!")
                    : RestResponse.fail("添加失败.");
    }
    @DeleteMapping("/delete/{permIds}")
    public RestResponse<String> removePerm(@PathVariable List<Integer> permIds){
        return permService.removePerms(permIds) ?
                RestResponse.success("删除成功!")
                    : RestResponse.fail("删除失败.");
    }
    @PostMapping("/update")
    public RestResponse<String> updatePerm(Perm perm){
        return permService.updateById(perm) ?
                RestResponse.success("保存成功!")
                    : RestResponse.fail("保存失败.");
    }
}
