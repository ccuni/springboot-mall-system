package com.star.ms.admin.service.product;

import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.entity.product.Category;

import java.util.List;

/**
* @author uni10
* @description 针对表【ms_category】的数据库操作Service
* @createDate 2022-05-22 13:28:37
*/
public interface CategoryService extends IService<Category> {

    List<Category> getAllCategory();
}
