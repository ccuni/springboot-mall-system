package com.star.ms.admin.service.impl.address;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.star.ms.common.dao.mapper.address.CityMapper;
import com.star.ms.common.entity.address.City;
import com.star.ms.admin.service.address.CityService;
import org.springframework.stereotype.Service;

/**
* @author uni10
* @description 针对表【ms_city】的数据库操作Service实现
* @createDate 2022-05-22 13:28:45
*/
@Service
public class CityServiceImpl extends ServiceImpl<CityMapper, City>
    implements CityService {

}




