package com.star.ms.admin.dao.impl.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.star.ms.admin.dao.user.AddressDAO;
import com.star.ms.common.dao.mapper.address.AreaMapper;
import com.star.ms.common.dao.mapper.address.CityMapper;
import com.star.ms.common.dao.mapper.address.ProvinceMapper;
import com.star.ms.common.entity.address.Area;
import com.star.ms.common.entity.address.City;
import com.star.ms.common.entity.address.Province;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class AddressDAOImpl implements AddressDAO {
    @Autowired
    private ProvinceMapper provinceMapper;
    @Autowired
    private CityMapper cityMapper;
    @Autowired
    private AreaMapper areaMapper;

    @Override
    public List<Province> selectProvinces() {
        return provinceMapper.selectList(null);
    }

    @Override
    public List<City> selectCities() {
        return cityMapper.selectList(null);
    }

    @Override
    public List<Area> selectAreas() {
        return areaMapper.selectList(null);
    }

    @Override
    public List<City> selectCityByProvinceCode(String provinceCode) {
        if("".equals(provinceCode)) return selectCities();
        LambdaQueryWrapper<City> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(City::getProvinceCode, provinceCode);
        return cityMapper.selectList(wrapper);
    }

    @Override
    public List<Area> selectAreaByCityCode(String cityCode) {
        if("".equals(cityCode)) return selectAreas();
        LambdaQueryWrapper<Area> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Area::getCityCode, cityCode);
        return areaMapper.selectList(wrapper);
    }

    @Override
    public Province selectProvinceByCode(String provinceCode) {
        LambdaQueryWrapper<Province> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Province::getCode, provinceCode);
        return provinceMapper.selectOne(wrapper);
    }

    @Override
    public City selectCityByCode(String cityCode) {
        LambdaQueryWrapper<City> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(City::getCode, cityCode);
        return cityMapper.selectOne(wrapper);
    }

    @Override
    public Area selectAreaByCode(String areaCode) {
        LambdaQueryWrapper<Area> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Area::getCode, areaCode);
        return areaMapper.selectOne(wrapper);
    }

    @Override
    public Province selectProvinceByName(String provinceName) {
        LambdaQueryWrapper<Province> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Province::getName, provinceName);
        return provinceMapper.selectOne(wrapper);
    }

    @Override
    public City selectCityByName(String cityName) {
        LambdaQueryWrapper<City> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(City::getName, cityName);
        return cityMapper.selectOne(wrapper);
    }

    @Override
    public Area selectAreaByNameAndCityCode(String areaName, String cityCode) {
        LambdaQueryWrapper<Area> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Area::getName, areaName);
        // 有出现重复的地区，所以要限制是哪一个城市的地区才行
        wrapper.eq(Area::getCityCode, cityCode);
        return areaMapper.selectOne(wrapper);
    }
}
