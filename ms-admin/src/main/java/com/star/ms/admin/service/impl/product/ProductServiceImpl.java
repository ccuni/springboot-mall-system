package com.star.ms.admin.service.impl.product;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.star.ms.common.dao.mapper.product.ProductMapper;
import com.star.ms.admin.dao.impl.product.ProductDAOImpl;
import com.star.ms.admin.service.product.ProductService;
import com.star.ms.common.entity.api.OssUploadResult;
import com.star.ms.common.entity.product.Category;
import com.star.ms.common.entity.product.Product;
import com.star.ms.common.service.impl.OssServiceImpl;
import com.star.ms.common.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
* @author uni10
* @description 针对表【ms_product】的数据库操作Service实现
* @createDate 2022-05-22 13:29:00
*/
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product>
    implements ProductService {
    @Autowired
    private ProductDAOImpl productDAO;
    @Autowired
    private OssServiceImpl ossService;

    @Override
    public List<Product> getProductWithTypeBySome(Integer pageNow,
                                                  Integer pageSize,
                                                  Map<String, Object> columnMap) {
        return productDAO.selectProductPageByMapWithType(pageNow, pageSize, columnMap);
    }

    @Override
    public IPage<Product> getProductPageWithTypeBySome(Integer pageNum, Integer pageSize, Map<String, Object> condition) {
        return productDAO.selectProductPageByMapWithType(new Page<>(pageNum, pageSize), condition);
    }

    @Override
    public Product getProductWithTypeAndProviderById(Long id) {
        return productDAO.selectWithCategoryAndProviderById(id);
    }

    @Override
    public Product getProductById(Long id) {
        return productDAO.selectProductById(id);
    }

    @Override
    public ProductVo getProductVoById(Long id) {
        ProductVo productVo = new ProductVo();
        Product product = productDAO.selectWithCategoryAndProviderById(id);
        productVo.setProduct(product);
        productVo.setProvider(product.getProvider());
        productVo.setSames(productDAO.selectProductByProviderId(
                product.getProvider().getId()).
               stream().filter(p-> !p.getId().equals(product.getId()))
               .collect(Collectors.toList()));
        return productVo;
    }
    public Category getCategoryByProductId(Long productId){
        return productDAO.selectCategoryByProductId(productId);
    }

    @Override
    public boolean updateCategoryById(Long productId, Long categoryId) {
        LambdaUpdateWrapper<Product> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(Product::getId, productId);
        wrapper.set(Product::getCategoryId, categoryId);
        return this.update(wrapper);
    }

    @Override
    public boolean saveImgById(Long productId, String img64Base) {
        // 调用 API 上传图片
        OssUploadResult ossResult = ossService.uploadProduct(productId, img64Base);
        return !"".equals(ossResult.getImgUrl()) && productDAO.updateImgById(productId, ossResult.getImgUrl());
    }

    @Override
    public boolean modifyProviderById(Long productId, Long providerId) {
        return productDAO.updateProviderById(productId, providerId);
    }

    @Override
    public boolean checkStock(long productId, int number) {
        LambdaUpdateWrapper<Product> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(Product::getId, productId);
        Product product = productDAO.selectProductById(productId);
        Long stock = product.getStock();
        if(stock >= number) {
            product.setStock(stock - number);
            return productDAO.updateById(product);
        }
        return false;
    }
}