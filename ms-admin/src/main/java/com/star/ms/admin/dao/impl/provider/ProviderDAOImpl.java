package com.star.ms.admin.dao.impl.provider;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.star.ms.admin.dao.provider.ProviderDAO;
import com.star.ms.common.dao.mapper.provider.ProviderMapper;
import com.star.ms.common.entity.provider.Provider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class ProviderDAOImpl implements ProviderDAO {
    @Autowired
    private ProviderMapper providerMapper;
    @Override
    public IPage<Provider> selectProductPageByMapWithType(Page<Provider> page, Map<String, Object> condition) {
        LambdaQueryWrapper<Provider> wrapper = new LambdaQueryWrapper<>();
        if(condition != null){
            for (Map.Entry<String, Object> entry : condition.entrySet()) {
                String key = entry.getKey();
                Object value = entry.getValue();
                if("name".equals(key)){
                    wrapper.like(Provider::getName, value);
                }
            }
        }
        return providerMapper.selectPage(page, wrapper);
    }

    @Override
    public Provider selectOneById(Long id) {
        LambdaQueryWrapper<Provider> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Provider::getId, id);
        return providerMapper.selectOne(wrapper);
    }
}
