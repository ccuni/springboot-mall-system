package com.star.ms.admin.service.impl.address;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.star.ms.common.dao.mapper.address.ProvinceMapper;
import com.star.ms.common.entity.address.Province;
import com.star.ms.admin.service.address.ProvinceService;
import org.springframework.stereotype.Service;

/**
* @author uni10
* @description 针对表【ms_province】的数据库操作Service实现
* @createDate 2022-05-22 13:29:15
*/
@Service
public class ProvinceServiceImpl extends ServiceImpl<ProvinceMapper, Province>
    implements ProvinceService {

}




