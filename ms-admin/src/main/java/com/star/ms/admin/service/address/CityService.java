package com.star.ms.admin.service.address;

import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.entity.address.City;

/**
* @author uni10
* @description 针对表【ms_city】的数据库操作Service
* @createDate 2022-05-22 13:28:45
*/
public interface CityService extends IService<City> {

}
