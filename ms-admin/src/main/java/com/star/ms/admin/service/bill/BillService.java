package com.star.ms.admin.service.bill;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.entity.Bill;

import java.util.List;
import java.util.Map;


public interface BillService extends IService<Bill> {

    IPage<Bill> getBillPageByCondition(int pageNum, int pageSize, Map<String, Object> condition);

    boolean updateStatus(List<Long> billIds, Integer status);

    Bill getBillWithAllById(Long billId);

    Bill addBillByMap(Map<String, Object> map);

    boolean payBillById(Long billId);

    boolean cancelBillById(Long billId);

    List<Bill> getCurrentBills(Integer num);

    Double countMoney();

    Map<String, Object> countMoneyByCategory();
}
