package com.star.ms.admin.service.user;

import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.entity.user.Ur;

/**
* @author uni10
* @description 针对表【ms_ur】的数据库操作Service
* @createDate 2022-05-22 13:29:33
*/
public interface UrService extends IService<Ur> {

}
