package com.star.ms.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.star.ms.admin.service.user.PermService;
import com.star.ms.admin.service.user.RoleService;
import com.star.ms.admin.service.user.UserService;
import com.star.ms.common.entity.RestResponse;
import com.star.ms.common.entity.product.Product;
import com.star.ms.common.entity.user.Perm;
import com.star.ms.common.entity.user.User;
import com.star.ms.common.vo.RolePermVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;
    @Autowired
    private PermService permService;

    /**
     * 获取所有角色的信息
     * @return
     */
    @PostMapping("/selectall")
    public String getAllRoleType(){
        return RestResponse.success(roleService.list()).toString();
    }

    /**
     * 获取所有的用户和角色
     */
    @PostMapping("/select")
    public String getAllUser(@RequestBody(required = false) Map<String, Object> param){
        IPage<Product> page = userService.getListWithRoleBySome(param);
        return RestResponse.success(page).toString();
    }
    @PostMapping("/update/{userIds}")
    public RestResponse<Object> modify(@PathVariable List<String> userIds, @RequestParam Integer roleId){

        return roleService.modifyUserRoleByCode(userIds, roleId) ?
                    RestResponse.success("角色修改成功") : RestResponse.fail("角色修改失败");
    }
    @PostMapping("/perm/selectall")
    public RestResponse<List<Perm>> getRoleWithPerm(){
        return RestResponse.success(permService.list());
    }
    @PostMapping("/perm/select")
    public RestResponse<IPage<RolePermVo>> getRoleWithPerm(@RequestBody(required = false) Map<String, Object> condition){
        return RestResponse.success(roleService.getRoleWithPermsByCondition(condition));
    }

    @PostMapping("/perm/select/{roleId}")
    public RestResponse<List<Perm>> getPermsByRoleId(@PathVariable Integer roleId){
        return RestResponse.success(permService.getByRoleId(roleId));
    }
    @DeleteMapping("/perm/delete")
    public RestResponse<String> removeRolePerm(@RequestBody Map<String, Object> map, HttpSession session){
        // 检查参数
        if(!(map.containsKey("roleIds") && map.containsKey("permIds")))
            return RestResponse.fail("传入的参数有误.");
        // 获取参数
        List<Integer> roleIds = (List<Integer>) map.get("roleIds");
        List<Integer> permIds = (List<Integer>) map.get("permIds");
        // 获取操作者的用户ID
        User user = (User) session.getAttribute("loginUser");
        boolean flag = roleIds.contains(user.getRole().getId());
        // 标记当前删除的权限中是否包括当前的用户角色
        return roleService.removeRolePerm(roleIds, permIds) ?
                (flag ? RestResponse.custom(201, "删除成功，请重新登录", null) :
                        RestResponse.success("删除成功")):
                RestResponse.fail("删除失败.");
    }
    @PostMapping("/insert")
    public RestResponse<String> addRole(@RequestParam String roleName, @RequestParam Integer roleExtendsId){

        return roleService.addRole(roleName, roleExtendsId) ?
                RestResponse.success("添加成功!") : RestResponse.fail("添加失败.");
    }
    @DeleteMapping("/delete/{roleId}")
    public RestResponse<String> removeRole(@PathVariable Integer roleId, HttpSession session){
        // 判断是否有当前的角色ID
        User loginUser = (User) session.getAttribute("loginUser");
        Integer rid = loginUser.getRole().getId();
        boolean flag = roleId.equals(rid);
        return roleService.removeRole(roleId) ? ( flag ?
                RestResponse.custom(201, "删除角色成功，请重新登录!", null): RestResponse.success("删除角色成功!")) :
                RestResponse.fail("删除角色失败.");
    }

    @PostMapping("/perm/insert")
    public RestResponse<String> addRole(@RequestParam Integer roleId, @RequestParam Integer permId){
        return roleService.addPerm(roleId, permId) ?
                RestResponse.success("添加成功!")
                    : RestResponse.fail("添加失败");
    }
}
