package com.star.ms.admin.dao.product;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.star.ms.common.entity.product.Category;
import com.star.ms.common.entity.product.Product;
import com.star.ms.common.vo.CategoryCountVo;

import java.util.List;
import java.util.Map;

public interface ProductDAO {
    public QueryWrapper<Product> produceWrapper(Map<String, Object> columnMap);

    public List<Product> selectProductByMap(Map<String, Object> columnMap);

    // 方式一：通过 wrapper 实现分页（缺点：一次查询的结果不包含商品类型
    public Page<Product> selectProductPageByMap(
            Integer pageNow,
            Integer pageSize,
            Map<String, Object> columnMap);

    // 方式二：通过 XML 实现分页查询 （缺点：包含类型但是耦合性低
    public List<Product> selectProductPageByMapWithType(
            Integer pageNow,
            Integer pageSize,
            Map<String, Object> columnMap);

    IPage<Product> selectProductPageByMapWithType(Page<Object> productPage, Map<String, Object> condition);

    Product selectWithCategoryAndProviderById(Long id);

    public Product selectProductById(Long id);

    List<Product> selectProductByProviderId(Long providerId);

    List<CategoryCountVo> countByCategory();

    Category selectCategoryByProductId(Long productId);

    boolean updateImgById(Long productId, String imgUrl);

    boolean updateProviderById(Long productId, Long providerId);

    boolean updateById(Product product);
}
