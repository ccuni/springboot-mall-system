package com.star.ms.admin.service.address;


import com.star.ms.common.entity.address.Area;
import com.star.ms.common.entity.address.City;
import com.star.ms.common.entity.address.Province;

import java.util.List;
import java.util.Map;

public interface AddressService {
    public List<Province> getProvinces();
    public List<City> getCities();
    public List<Area> getAreas();

    // 按照上级查询
    public List<City> getCityByProvinceCode(String provinceCode);
    public List<Area> getAreaByCityCode(String cityCode);

    public Map<String, String> getAddressByCode(String userCode);

}
