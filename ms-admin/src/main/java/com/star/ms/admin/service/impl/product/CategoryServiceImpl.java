package com.star.ms.admin.service.impl.product;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.star.ms.common.dao.mapper.product.CategoryMapper;
import com.star.ms.common.entity.product.Category;
import com.star.ms.admin.service.product.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
* @author uni10
* @description 针对表【ms_category】的数据库操作Service实现
* @createDate 2022-05-22 13:28:37
*/
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category>
    implements CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;
    @Override
    public List<Category> getAllCategory() {
        return categoryMapper.selectAllWithCount();
    }
}




