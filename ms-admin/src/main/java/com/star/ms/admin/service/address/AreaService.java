package com.star.ms.admin.service.address;

import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.entity.address.Area;

/**
* @author uni10
* @description 针对表【ms_area】的数据库操作Service
* @createDate 2022-05-22 13:28:21
*/
public interface AreaService extends IService<Area> {

}
