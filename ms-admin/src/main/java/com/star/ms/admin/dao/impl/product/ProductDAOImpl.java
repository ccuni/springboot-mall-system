package com.star.ms.admin.dao.impl.product;

/*
 * @Author: Uni
 * @Time: 2022/5/10
 * @TODO
 */

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.star.ms.admin.dao.product.ProductDAO;
import com.star.ms.common.dao.mapper.product.ProductMapper;
import com.star.ms.common.enums.ProductStatusEnum;
import com.star.ms.common.entity.product.Category;
import com.star.ms.common.entity.product.Product;

import com.star.ms.common.vo.CategoryCountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ProductDAOImpl implements ProductDAO {
    @Autowired
    private ProductMapper productMapper;

    /**
     * 根据 map 产生 wrapper
     *
     * @param columnMap 限制查询的条件
     * @return wrapper 包装
     */
    public QueryWrapper<Product> produceWrapper(Map<String, Object> columnMap) {
        QueryWrapper<Product> wrapper = new QueryWrapper<>();
        if (columnMap != null) {
            for (Map.Entry<String, Object> entry : columnMap.entrySet()) {
                String key = entry.getKey();
                Object val = entry.getValue();
                // 限制价格
                if (ProductStatusEnum.MIN_PRICE.getVal().equals(key))
                    wrapper.ge("price", Float.parseFloat(val.toString()));
                else if (ProductStatusEnum.MAX_PRICE.getVal().equals(key))
                    wrapper.le("price", Float.parseFloat(val.toString()));
                    // 根据商品名称查询
                else if (ProductStatusEnum.NAME.getVal().equals(key))
                    wrapper.like("name", val.toString());
            }
        }
        return wrapper;
    }

    public List<Product> selectProductByMap(Map<String, Object> columnMap) {
        return productMapper.selectList(produceWrapper(columnMap));
    }

    // 方式一：通过 wrapper 实现分页（缺点：一次查询的结果不包含商品类型
    public Page<Product> selectProductPageByMap(
            Integer pageNow,
            Integer pageSize,
            Map<String, Object> columnMap) {
        return productMapper.selectPage(new Page<>(pageNow, pageSize), produceWrapper(columnMap));
    }
    // 方式二：通过 XML 实现分页查询 （缺点：包含类型但是耦合性低

    public List<Product>  selectProductPageByMapWithType(
            Integer pageNow,
            Integer pageSize,
            Map<String, Object> columnMap) {
        return productMapper.selectProductByMap(new Page<>(pageNow, pageSize), columnMap);
    }

    @Override
    public IPage<Product> selectProductPageByMapWithType(Page<Object> productPage, Map<String, Object> condition) {
        return productMapper.selectProductPageByMap(productPage, condition);
    }

    @Override
    public Product selectWithCategoryAndProviderById(Long id) {
        return productMapper.selectWithCategoryAndProviderById(id);
    }

    public Product selectProductById(Long id) {
        LambdaQueryWrapper<Product> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Product::getId, id);
        return productMapper.selectOne(wrapper);
    }

    @Override
    public List<Product> selectProductByProviderId(Long providerId) {
        LambdaQueryWrapper<Product> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(Product::getProviderId, providerId);
        return productMapper.selectList(wrapper);
    }

    @Override
    public  List<CategoryCountVo> countByCategory() {
        return productMapper.countByCategory();
    }

    @Override
    public Category selectCategoryByProductId(Long productId) {
        return productMapper.selectCategoryByProductId(productId);
    }

    @Override
    public boolean updateImgById(Long productId, String imgUrl) {
        return productMapper.updateImgById(productId, imgUrl);
    }

    @Override
    public boolean updateProviderById(Long productId, Long providerId) {
        UpdateWrapper<Product> wrapper = new UpdateWrapper<>();
        wrapper.eq("product_id", productId).set("product_provider_id", providerId);
        return productMapper.update(null, wrapper) > 0;
    }

    @Override
    public boolean updateById(Product product) {
        return productMapper.updateById(product) > 0;
    }

}
