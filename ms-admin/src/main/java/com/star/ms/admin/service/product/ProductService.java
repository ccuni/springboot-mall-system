package com.star.ms.admin.service.product;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.entity.product.Category;
import com.star.ms.common.entity.product.Product;
import com.star.ms.common.vo.ProductVo;

import java.util.List;
import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_product】的数据库操作Service
* @createDate 2022-05-22 13:29:00
*/
public interface ProductService extends IService<Product> {
    // 根据限制条件来查找商品信息（包括商品的类型）
    public List<Product> getProductWithTypeBySome(Integer pageNow,
                                                  Integer pageSize,
                                                  Map<String, Object> columnMap);
    public IPage<Product> getProductPageWithTypeBySome(Integer pageNow,
                                                       Integer pageSize,
                                                       Map<String, Object> columnMap);

    Product getProductWithTypeAndProviderById(Long id);

    Product getProductById(Long id);

    ProductVo getProductVoById(Long id);
    public Category getCategoryByProductId(Long productId);

    boolean updateCategoryById(Long productId, Long categoryId);

    boolean saveImgById(Long productId, String imgUrl);

    boolean modifyProviderById(Long productId, Long providerId);

    boolean checkStock(long productId, int number);
}
