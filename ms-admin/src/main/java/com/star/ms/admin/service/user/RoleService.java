package com.star.ms.admin.service.user;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.star.ms.common.entity.user.Role;
import com.star.ms.common.vo.RolePermVo;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_role】的数据库操作Service
* @createDate 2022-05-22 13:29:22
*/
@Service
public interface RoleService extends IService<Role> {

    boolean modifyUserRoleByCode(List<String> userIds, Integer roleId);
    Role getByUserCode(String userCode);

    IPage<RolePermVo> getRoleWithPermsByCondition(Map<String, Object> condition);

    boolean removeRolePerm(List<Integer> roleIds, List<Integer> permIds);

    boolean addRole(String roleName, Integer roleExtendsId);

    boolean removeRole(Integer roleId);

    boolean addPerm(Integer roleId, Integer permId);
}
