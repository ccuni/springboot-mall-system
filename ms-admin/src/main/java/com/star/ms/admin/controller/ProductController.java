package com.star.ms.admin.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.star.ms.admin.service.product.ProductService;
import com.star.ms.common.entity.RestResponse;
import com.star.ms.common.entity.product.Product;
import com.star.ms.common.vo.ProductVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/product")
public class ProductController {
    @Autowired
    private ProductService productService;

    /**
     * 查询商品信息
     * @param param
     * @return
     */
    @PostMapping ("/query")
    public String getProduct(@RequestBody(required = false) Map<String, Object> param){
        int pageNum = 1;
        int pageSize = 8;
        // 处理空参
        if(param != null){
            pageNum = Integer.parseInt(param.getOrDefault("pageNum", 1).toString());
            pageSize = Integer.parseInt(param.getOrDefault("pageSize", 8).toString());
        }
        IPage<Product> page = productService.getProductPageWithTypeBySome(pageNum, pageSize, param);
        return RestResponse.success(page).toString();
    }

    /**
     * 查询商品详情、商品分类和供应商
     * @param id
     * @return
     */
    @PostMapping("/query/{id}")
    public RestResponse<ProductVo> getProductById(@PathVariable Long id){
        return RestResponse.success(productService.getProductVoById(id));
    }
    @PostMapping("/category/update/{productId}")
    public RestResponse<String> updateCategory(@PathVariable Long productId, Long categoryId){
        return productService.updateCategoryById(productId, categoryId) ?
                RestResponse.success("修改成功"):
                    RestResponse.fail("修改失败.");
    }
    @PutMapping("/img/save/{productId}")
    public RestResponse<String> updateImg(@PathVariable Long productId, @RequestParam String img64Base){
        return  productService.saveImgById(productId, img64Base) ?
                RestResponse.success("修改成功!"):
                        RestResponse.fail("修改失败.");
    }
    @PostMapping("/update")
    public RestResponse<String> updateProduct(Product product){
        System.out.println("修改的商品: " + product);
        return productService.updateById(product) ?
                RestResponse.success("修改成功!")
                : RestResponse.success("修改失败.");
    }

    @PostMapping("/provider/update/{productId}")
    public RestResponse<String> updateProduct(@PathVariable Long productId, @RequestParam Long providerId){
        return productService.modifyProviderById(productId, providerId) ?
                RestResponse.success("修改成功") : RestResponse.fail("修改失败");
    }
    @DeleteMapping("/delete/{productIds}")
    public RestResponse<String> deleteProduct(@PathVariable List<Long> productIds){
        return productService.removeBatchByIds(productIds) ?
                RestResponse.success("删除成功!")
                : RestResponse.fail("删除失败");
    }
    @PostMapping("/insert")
    public RestResponse<String> insertProduct(Product product, String img){
        System.out.println("添加的商品信息: " + product);
        System.out.println("商品图片: " + img);
        product.setImg(img);
        return  productService.save(product) ?
                RestResponse.success("添加成功!")
                    : RestResponse.fail("添加失败");
    }
}