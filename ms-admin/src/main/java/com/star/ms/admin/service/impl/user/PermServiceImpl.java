package com.star.ms.admin.service.impl.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.star.ms.common.dao.mapper.user.PermMapper;
import com.star.ms.common.entity.user.Perm;
import com.star.ms.admin.service.user.PermService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
* @author uni10
* @description 针对表【ms_perm】的数据库操作Service实现
* @createDate 2022-05-22 13:28:53
*/
@Service
public class PermServiceImpl extends ServiceImpl<PermMapper, Perm>
    implements PermService {
    @Autowired
    private PermMapper permMapper;
    @Override
    public List<Perm> getByRoleId(Integer roleId) {
        return permMapper.selectByRoleId(roleId);
    }

    @Override
    public IPage<Perm> getPermsPageByMap(Map<String, Object> condition) {
        int pageNum = 1;
        int pageSize = 5;
        if(condition != null){
            pageNum = Integer.parseInt(condition.getOrDefault("pageNum", 1).toString());
            pageSize = Integer.parseInt(condition.getOrDefault("pageSize", 5).toString());
        }
        return permMapper.selectPageByMap(new Page<>(pageNum, pageSize), condition);
    }

    @Override
    public boolean addPerm(String name, String url) {
        Perm perm = new Perm();
        perm.setName(name);
        perm.setUrl(url);
        return permMapper.insert(perm) > 0;
    }

    @Override
    public boolean removePerms(List<Integer> permIds) {
        LambdaQueryWrapper<Perm> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(Perm::getId, permIds);
        return permMapper.delete(wrapper) > 0;
    }
}




