package com.star.ms.admin.dao.user;

import com.star.ms.common.entity.address.Area;
import com.star.ms.common.entity.address.City;
import com.star.ms.common.entity.address.Province;


import java.util.List;

public interface AddressDAO {
    // 查询所有
    public List<Province> selectProvinces();
    public List<City> selectCities();
    public List<Area> selectAreas();

    // 按照上级查询
    public List<City> selectCityByProvinceCode(String provinceCode);
    public List<Area> selectAreaByCityCode(String cityCode);

    // 根据 code 查询
    public Province selectProvinceByCode(String provinceCode);
    public City selectCityByCode(String cityCode);
    public Area selectAreaByCode(String areaCode);

    Province selectProvinceByName(String provinceName);

    City selectCityByName(String cityName);

    Area selectAreaByNameAndCityCode(String areaName, String cityCode);
}
