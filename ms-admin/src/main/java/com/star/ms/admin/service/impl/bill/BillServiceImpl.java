package com.star.ms.admin.service.impl.bill;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.star.ms.admin.service.bill.BillService;
import com.star.ms.admin.service.product.CategoryService;
import com.star.ms.admin.service.product.ProductService;
import com.star.ms.common.dao.mapper.bill.BillMapper;
import com.star.ms.common.entity.Bill;
import com.star.ms.common.entity.product.Category;
import com.star.ms.common.entity.product.Product;
import com.star.ms.common.utils.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@Service
public class BillServiceImpl extends ServiceImpl<BillMapper, Bill>
        implements BillService {
    @Autowired
    private BillMapper billMapper;
    @Autowired
    private ProductService productService;
    @Autowired
    private CategoryService categoryService;
    @Override
    public IPage<Bill> getBillPageByCondition(int pageNum, int pageSize, Map<String, Object> condition) {
        return billMapper.selectWithBuyerAndProviderByPage(new Page<>(pageNum, pageSize), condition);
    }

    @Override
    public boolean updateStatus(List<Long> billIds, Integer status) {
        UpdateWrapper<Bill> wrapper = new UpdateWrapper<>();
        wrapper.in("bill_id", billIds).set("bill_status", status);
        return this.update(wrapper);
    }

    @Override
    public Bill getBillWithAllById(Long billId) {
        return billMapper.selectWithBuyerAndProviderAndProduct(billId);
    }

    @Override
    public Bill addBillByMap(Map<String, Object> map) {
        long productId = (int)map.get("productId");
        long providerId = (int) map.get("providerId");
        int number = (int) map.get("number");
        double price = Double.parseDouble(map.get("price").toString());
        System.out.println("productId: "+ productId);
        System.out.println("providerId: " + providerId);
        System.out.println("number: " + number);
        Bill bill = new Bill();
        bill.setProductId(productId);
        bill.setPrice(price);
        bill.setProviderId(providerId);
        bill.setNumber(number);
        // 确认存货是否足够(足够的话直接减)
        if(productService.checkStock(productId, number)){
            // 存货足够后在插入订单
            bill.setCreateTime(DateUtil.now());
            return  save(bill) ? bill : null;
        }
        else return null;
    }

    /**
     * 支付订单
     * @param billId 订单号
     * @return 支付的结果
     */
    @Override
    public boolean payBillById(Long billId) {
        // 查询订单
        Bill bill = this.getBillWithAllById(billId);
        // 验证商品库存是否足够
        boolean flag = productService.checkStock(bill.getProductId(), bill.getNumber());
        // 商品足够的话 ，保持商品库存量的一致性
        if(flag){
            Product product = bill.getProduct();
            // 计算购买后的库存量
            product.setStock(product.getStock() - bill.getNumber());
            // 最后更新订单状态
            bill.setStatus(1);
            // 记录结果
            flag = productService.updateById(product);
            flag &= billMapper.updateById(bill) > 0;
        }
        return flag;
    }
    @Override
    public boolean cancelBillById(Long billId) {
        Bill bill = this.getBillWithAllById(billId);
        return this.removeById(bill);
    }

    @Override
    public List<Bill> getCurrentBills(Integer num) {
        return billMapper.selectCurrentBills(num);
    }

    @Override
    public Double countMoney() {
        return billMapper.selectCountMoney();
    }


    /**
     * 根据商品的类型统计交易额
     * @return
     */
    @Override
    public Map<String, Object> countMoneyByCategory() {
        // 找出所有不同商品类型
        List<Category> categoryList = categoryService.list();
        List<Float> moneyList = new ArrayList<>(categoryList.size());
        // 统计每个商品类型的交易额
        for (Category category : categoryList) {
            Float sum = billMapper.selectCountMoneyByCategoryId(category.getId());
            moneyList.add(sum == null ? 0f : sum);
        }
        List<Map<String, Object>> list = new ArrayList<>();
        assert categoryList.size() == moneyList.size();
        for(int i = 0; i < categoryList.size(); i++){
            HashMap<String, Object> map = new HashMap<>();
            map.put("name", categoryList.get(i).getName());
            map.put("value", moneyList.get(i));
            list.add(map);
        }
        return new HashMap<String, Object>(){{
            put("categoryName", categoryList);
            put("categoryCountMoney", moneyList);
            put("categoryMoneyMap", list);
        }};
    }
}
